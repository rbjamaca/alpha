<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'auth'], function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout'])->middleware('auth:api');
    Route::post('register', [AuthController::class, 'register']);
    // Route::post('password/email', 'Api\ForgotPasswordController@sendResetLinkEmail');
    // Route::post('password/reset', 'Api\ResetPasswordController@reset');    
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// API Authentication Routes

Route::group(['middleware' => ['auth:api'], 'prefix' => 'admin'], function () {
    Route::get('/code', [AdminController::class, 'index']);
    Route::post('/code', [AdminController::class, 'store']);
    Route::post('/codes', [AdminController::class, 'store_batch']);
    Route::get('/purchases', [AdminController::class, 'purchases']);
    Route::post('/purchases', [AdminController::class, 'savePurchase']);
    Route::get('/search-by-id/{id}', [AdminController::class, 'searchByIdNumber']);
    Route::put('/code/{id}', [AdminController::class, 'assign']);
    Route::get('/gold/requests', [AdminController::class, 'getRequests']);
    Route::put('/gold/{id}', [AdminController::class, 'upgradeGold']);
});
// User Routes
Route::group(['middleware' => ['auth:api'], 'prefix' => 'user'], function () {
    Route::get('/', [UserController::class, 'index']);
    Route::get('/members', [UserController::class, 'members']);
    Route::get('/sponsored', [UserController::class, 'sponsored']);
    Route::get('/{id}', [UserController::class, 'getUser']);
    Route::get('/downlines/{id}', [UserController::class, 'downlines']);
    Route::get('/dash/metrics', [UserController::class, 'metrics']);
    Route::post('/membership/upgrade', [UserController::class, 'upgrade']);
    Route::get('/search-by-id/{id}/{up}', [AdminController::class, 'searchByIdNumber']);
});
