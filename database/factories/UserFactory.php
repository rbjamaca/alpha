<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'firstname' => $this->faker->name,
            'username' => 'Admin2021',
            'id_number' => 'AM-11337-0',
            'email' => 'admin@gmail.com',
            'email_verified_at' => now(),
            'admin' => "admin",
            'password' => '$2y$12$NXB.QoNir1NcgaejTIGiC.TjjEAQptcNXosjKnma/mZwF86TFmTKa',
            'remember_token' => Str::random(10),
        ];
    }
}
