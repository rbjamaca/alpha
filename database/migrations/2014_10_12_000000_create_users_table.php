<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('id_number')->unique()->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('admin')->default("user");
            $table->string('firstname')->nullable();
            $table->string('surname')->nullable();
            $table->string('midname')->nullable();
            $table->string('address')->nullable();
            $table->string('gender')->nullable();
            $table->string('civil')->nullable();
            $table->string('birthmonth')->nullable();
            $table->string('birthday')->nullable();
            $table->string('birthyear')->nullable();
            $table->string('cell')->nullable();
            $table->string('username')->nullable();
            $table->string('sponsor')->nullable();
            $table->string('code')->nullable();
            // $table->string('type')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
