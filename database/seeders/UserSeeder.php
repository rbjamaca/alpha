<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(0,0) as $index) {
            $email = ['user1@gmail.com'];
                DB::table('users')->insert([
                    'firstname' => $faker->firstName,
                    'surname' => $faker->lastName,
                    'username' => 'MasterBuyo2020',
                    'email' => $email[$index],
                    'email_verified_at' => now(),
                    'id_number' => 'AM-11337-1',
                    'sponsor' => 'AM-11337-0',
                    'admin' => 0,
                    'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                    'created_at' => now(),
                    'updated_at' => now(),
                ]);
        }
    }
}
