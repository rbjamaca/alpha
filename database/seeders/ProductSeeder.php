<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('set_products')->truncate();
        DB::table('sets')->truncate();
        DB::table('products')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        $faker = Faker::create();
        foreach (range(0,1) as $index) {
            $product = ['Paragis Coffee', 'Masterbuyo Herbal Oil'];
                DB::table('products')->insert([
                    'name' => $product[$index],
                    'created_at' => now(),
                    'updated_at' => now(),
                ]);
        }

        DB::table('sets')->insert([
            'desc' => 'Masterbuyo Herbal Oil + Paragis Coffee Set',
            'price' => 695,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        foreach (range(0,1) as $index) {
                $container = ['Box', 'Btl'];
                DB::table('set_products')->insert([
                    'product_id' => $index+1,
                    'set_id' => 1,
                    'amount' => 1,
                    'container' => $container[$index],
                    'created_at' => now(),
                    'updated_at' => now(),
                ]);
        }
    }
}
