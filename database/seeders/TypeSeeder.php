<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::with('codes')->where('admin', 'user')->get();

        foreach ($users as $value) {
            if($value->codes){
                if($value->codes->type == "Gold"){
                    $code = "Gold";
                }elseif ($value->codes->type == "Free Code") {
                    $code = "Free";
                }elseif ($value->codes->type == "CD Code") {
                    $code = "CD";
                }else{
                    $code = "Pre-Membership";
                }
            }
            DB::table('users')->where('id', $value->id)
            ->update(array('status' => $code));
        }
    }
}
