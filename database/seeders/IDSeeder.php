<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class IDSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        $users_arr = $users->pluck('id');

        foreach ($users_arr as $value) {
            $id_number = +$value - 1;
            $number = 'AM-11337-'.$id_number;
            DB::table('users')->where('id', $value)
            ->update(array('id_number' => $number));
        }
    }
}
