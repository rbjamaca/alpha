<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::with('uplineRecursive')->get();
        foreach ($users as $u) {
            if($u->admin == "admin" || $u->sponsor == "Admin2021"){

            }else{
                $sponsor = User::whereHas('df', function($q) use($u){
                    return $q->where('id', $u->id);
                })->first();
                $pos = $this->recursive(collect($u), $sponsor->id);
                // dd($pos);
                DB::table('users')->where('id', $u->id)
                ->update(array('position' => $pos['position']));
            }
            
        }
    }

    public function recursive($up, $id)
    {
        if(!isset($up['sponsor_id'])){
            return $this->recursive($up['upline_recursive'], $id);
        }else{
            if($up['sponsor_id']){
                if($up['sponsor_id'] != $id){
                    return $this->recursive($up['upline_recursive'], $id);
                }else{
                    return $up;
                }
            }
        }
        
    }
}
