<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BuySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        $users_arr = $users->pluck('id');

        foreach ($users_arr as $value) {
            DB::table('purchases')->insert([
                'user_id' => $value,
                'set_id' => 1,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
