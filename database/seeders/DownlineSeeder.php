<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DownlineSeeder extends Seeder
{
    public function run()
    {
        $users = User::with('uplineRecursive')->get();
        foreach ($users as $u) {
            if($u->admin == "admin" || $u->sponsor == "Admin2021" || $u->id == 2){

            }else{
                $sponsor = 2;
                $pos = $this->recursive(collect($u), $sponsor, $u->id, $u->created_at);
                // DB::table('users')->where('id', $u->id)
                // ->update(array('position' => $pos['position']));
            }
        }
    }

    public function recursive($up, $id, $user, $date)
    {
        if(!isset($up['sponsor_id'])){
            return $this->recursive($up['upline_recursive'], $id, $user, $date);
        }else{
            if($up['sponsor_id']){
                DB::table('user_user')->insert([
                    'user_id' => $user,
                    'parent_id' => $up['sponsor_id'],
                    'position' => $up['position'],
                    'created_at' => Carbon::parse($date)->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::parse($date)->format('Y-m-d H:i:s')
                ]);           
                if($up['sponsor_id'] != $id){
                    return $this->recursive($up['upline_recursive'], $id, $user, $date);
                }else{
                    return $up;
                }
            }
        }
        
    }
}
