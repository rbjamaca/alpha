<?php

namespace App\Http\Controllers;

use App\Models\Code;
use App\Models\Purchase;
use App\Models\Set;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        $code = Code::with('user')->orderBy('created_at', 'DESC')->get();

        return response()->json([
            'list' => $code,
        ]);
    }

    public function store(Request $request)
    {
        $code = new Code();
        $code->code = $request->code;
        $code->type = $request->type;
        $code->prefix = $request->prefix;
        $code->status = 'Generated';
        $code->save();
        return response()->json([
            'info' => $code,
            'message' => 'success',
        ], 201);
    }

    public function store_batch(Request $request)
    {
        $codes = collect($request->codes)->map(function($c) use($request){
            return collect(['code' => $c, 'type' => $request->type, 'prefix' => $request->prefix, 'status' => 'Assigned', 'created_at' => now()]);
        });
    
        $save = Code::insert($codes->toArray());

        return response()->json([
            'info' => $save,
            'message' => 'success',
        ], 201);
    }

    public function savePurchase(Request $request)
    {
        $validatedData = $request->validate([
            'id' => ['required'],
        ]);
        
        $purch = new Purchase();
        $purch->user_id = $request->id;
        $purch->set_id = $request->set;
        $purch->save();
        return response()->json([
            'info' => $purch,
            'message' => 'success',
        ], 201);
    }

    public function purchases()
    {
        $purch = Purchase::with(['set', 'user'])->orderBy('created_at', 'DESC')->get();
        $sets = Set::with(['set_products.product'])->orderBy('created_at', 'DESC')->get();

        return response()->json([
            'list' => $purch,
            'sets' => $sets,
        ]);
    }

    public function recursive($up, $id)
    {
        if(!isset($up['sponsor_id'])){
            if(!$up['upline_recursive']){
                return $up;
            }
            return $this->recursive($up['upline_recursive'], $id);
        }else{
            if($up['sponsor_id']){
                if($up['sponsor_id'] == "1"){
                    return $up;
                }
                else if($up['sponsor_id'] != $id){
                    return $this->recursive($up['upline_recursive'], $id);
                }else{
                    return $up;
                }
            }
        }
        
    }

    public function searchByIdNumber($id, $up)
    {
        $user = User::where('id_number', $id)->first();
        $up = User::with(['uplineRecursive'])->find($up);

        $position = $this->recursive(collect($up), $user->id);

        return response()->json([
            'info' => $user,
            'pos' => $position,
        ]);
    }

    public function assign(Request $request, $id)
    {
        $code = Code::find($id);

        $code->status = $request->status;
        $code->save();

        return response()->json([
            'info' => $code,
            'message' => 'Successful'
        ]);
    }

    public function getRequests()
    {
        $users = User::where('status', 'Pending for Gold')->get();
        
        return response()->json([
            'list' => $users,
        ]);
    }

    public function upgradeGold(Request $request, $id)
    {
        $user = User::find($id);
        $code = new Code();
        $code->code = $request->code;
        $code->type = "Gold";
        $code->status = "Used";
        $code->save();

        $user->code = $request->code;
        $user->status = "Gold";
        $str_to_replace = $code->prefix;
        $input_str = $user->id_number;
        $output_str = $str_to_replace.substr($input_str, 2);
        $user->id_number = $output_str;
        $user->save();
        
        return response()->json([
            'info' => $user,
        ]);
    }
}
