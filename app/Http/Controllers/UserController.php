<?php

namespace App\Http\Controllers;

use App\Models\Downline;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function index() {
        $user = Auth::user();
        $nested = User::with(['nestedDownlines', 'df', 'upline', 'children', 'parent_dr'])
            ->withCount(['children as cd_left' => function($q){
                return $q->where('user_user.position', 'L')->whereHas('code', function($q){
                    $q->where('prefix', 'CD');
                });
            }, 'children as cd_right' => function($q){
                return $q->where('user_user.position', 'R')->whereHas('code', function($q){
                    $q->where('prefix', 'CD');
                });
            }, 'children as fr_left' => function($q){
                return $q->where('user_user.position', 'L')->whereHas('code', function($q){
                    $q->where('prefix', 'FR');
                });
            }, 'children as fr_right' => function($q){
                return $q->where('user_user.position', 'R')->whereHas('code', function($q){
                    $q->where('prefix', 'FR');
                });
            }, 'children as gl_left' => function($q){
                return $q->where([['user_user.position', 'L'], ['isGold', true]]);
            }, 'children as gl_right' => function($q){
                return $q->where([['user_user.position', 'R'], ['isGold', true]]);
            }, 'children as pr_left' => function($q){
                return $q->where([['isGold', false], ['user_user.position', 'L']])->whereHas('code', function($q){
                    return $q->where([['type', 'Pre-Membership']]);
                });
            }, 'children as pr_right' => function($q){
                return $q->where([['isGold', false], ['user_user.position', 'R']])
                    ->whereHas('code', function($q){
                        return $q->where([['type', 'Pre-Membership']]);
                    });
            }, 'children as total'])->find($user->id);
        return response()->json([
            'user' => $user,
            'list' => $nested,
        ]);
    }

    public function members()
    {
        $users = User::latest()->get();
        
        return response()->json([
            'list' => $users,
        ]);
    }

    public function sponsored()
    {
        $user = Auth::user();
        $users = User::with('df')->find($user->id);
        
        return response()->json([
            'list' => $users->df,
        ]);
    }

    private function recursive($nested, $level, $new_arr = [])
    {
        $rows = $new_arr;
        $children = $nested;
        $lvl = collect(['level' => $level]);
        if(!empty($children) 
        // && $level < 10
        ){
            foreach ($children as $child){
                if($child['status'] == "Gold" || $child['status'] == "Pre-Membership"){
                    $new = collect($child)->except(['nested_dr'])->toArray() + $lvl->toArray();
                    array_push($rows, $new);
                    $lv = $level + 1;
                }
                $rows = $this->recursive($child['nested_dr'], $lv ?? $level, $rows);
            }
        }
        return $rows;
    }

    private function organizer($nested, $level, $new_arr = [])
    {
        $rows = null;
        $children = $nested;
        $lvl = collect(['level' => $level]);
        if(!empty($children) 
        // && $level < 10
        ){
            foreach ($children as $child){
                $rows = $new_arr;
                if($child['status'] == "Gold" || $child['status'] == "Pre-Membership" && $level > 100){
                    $new = collect($child)->except(['nested_dr'])->toArray() + $lvl->toArray();
                    array_push($rows, $new);
                    $lv = $level + 1;
                }
                $rows = $this->recursive($child['nested_dr'], $lv ?? $level, $rows);
            }
        }
        return $rows;
    }

    private function achiever($nested, $level, $new_arr = [])
    {
        $rows = null;
        $children = $nested;
        $lvl = collect(['level' => $level]);
        if(!empty($children) 
        // && $level < 10
        ){
            foreach ($children as $child){
                $rows = $new_arr;
                if($child['status'] == "Gold" || $child['status'] == "Pre-Membership" && $level > 200){
                    $new = collect($child)->except(['nested_dr'])->toArray() + $lvl->toArray();
                    array_push($rows, $new);
                    $lv = $level + 1;
                }
                $rows = $this->recursive($child['nested_dr'], $lv ?? $level, $rows);
            }
        }
        return $rows;
    }

    public function getUser($id) {
        $nested = User::with(['nestedDownlines', 'upline', 'uplineRecursive', 'children', 'parent_dr', 'children'])->withCount(['children as cd_left' => function($q){
            return $q->where('user_user.position', 'L')->whereHas('code', function($q){
                $q->where('prefix', 'CD');
            });
        }, 'children as cd_right' => function($q){
            return $q->where('user_user.position', 'R')->whereHas('code', function($q){
                $q->where('prefix', 'CD');
            });
        }, 'children as fr_left' => function($q){
            return $q->where('user_user.position', 'L')->whereHas('code', function($q){
                $q->where('prefix', 'FR');
            });
        }, 'children as fr_right' => function($q){
            return $q->where('user_user.position', 'R')->whereHas('code', function($q){
                $q->where('prefix', 'FR');
            });
        }, 'children as gl_left' => function($q){
            return $q->where([['user_user.position', 'L'], ['isGold', true]]);
        }, 'children as gl_right' => function($q){
            return $q->where([['user_user.position', 'R'], ['isGold', true]]);
        }, 'df as pr_left' => function($q){
            return $q->where([['isGold', false], ['position', 'L']])->whereHas('code', function($q){
                return $q->where([['type', 'Pre-Membership']]);
            });
        }, 'df as pr_right' => function($q){
            return $q->where([['isGold', false], ['position', 'R']])
                ->whereHas('code', function($q){
                    return $q->where([['type', 'Pre-Membership']]);
                });
        }, 'children as total'])->find($id);
        return response()->json([
            'list' => $nested,
        ]);
    }

    public function downlines($id)
    {
        $nested = User::with('nestedDownlines')->find($id);
        return response()->json([
            'list' => $nested,
        ]);
    }

    public function metrics()
    {
        $user = Auth::user();
        $matching = User::withCount(['children as left' => function($q){
                    return $q->where('user_user.position', 'L')
                            ->whereHas('code', function($q){
                            $q->where('type', 'Gold');
                        });
                    }, 'children as right' => function($q){
                        return $q->where('user_user.position', 'R')
                            ->whereHas('code', function($q){
                            $q->where('type', 'Gold');
                        });
                    }, 'df as total' => function($q){
                        return $q->whereHas('code', function($q){
                            $q->where('type', 'Gold')->orWhere('type', 'Pre-Membership');
                        });
                    }])->with(['nested_dr' => function($q){
                        return $q->whereHas('code', function($q){
                            $q->where('type', 'Gold')->orWhere('type', 'Pre-Membership');
                        });
                    }])->find($user->id);

        // $unl = User::withCount(['children as gold' => function($q){
        //                             return $q->where('isGold', true)->limit(10);
        //                         }, 'children as prem' => function($q){
        //                             return $q->whereIn('status', ['Pre-Membership', 'Pending for Gold'])->limit(10);
        //                         }])->find($user->id);
        $organizer_all = collect($this->organizer($matching->nested_dr, 1))->sortBy('level');
        $achiever_all = collect($this->achiever($matching->nested_dr, 1))->sortBy('level');
        $organizer = collect($organizer_all)->count() ?? 0 * 25;
        $achiever = collect($achiever_all)->count() ?? 0 * 25;

        $unilevel = User::withCount(['unilevel as prem' => function($q){
            return $q->where([['isGold', false]])->whereHas('code', function($q){
                return $q->where('type', 'Pre-Membership');
            });
        }, 'unilevel as gold' => function($q){
            return $q->where([['isGold', true]]);
        }])->with(['df', 'unilevel'])->find($user->id);

        $dr_gold = User::where([['sponsor', $user->id_number], ['isGold', true]])->whereHas('code', function($q){
                        $q->where('type', 'Gold');
                    })->get()->count();

        $dr_pre = User::where([['sponsor', $user->id_number], ['isGold', false]])->whereHas('code', function($q){
                        $q->whereNotIn('type', ['CD Code', 'Free Code']);
                    })->get()->count();

        // $dr = User::withCount(['children as gold' => function($q){
        //                 return $q->where([['isGold', true]]);
        //             }, 'df as prem' => function($q){
        //                 return $q->where([['isGold', false]])
        //                     ->whereHas('code', function($q){
        //                         return $q->where([['type', 'Pre-Membership']]);
        //                     });
        //             }])->find($user->id);

        $df = ($dr_gold * 500) + ($dr_pre * 50);

        return response()->json([
            'user' => $user,
            'unilevel' => $unilevel,
            'status' => $user->status,
            'df' => $df,
            'matching' => $matching,
            'organizer' => $organizer,
            'achiever' => $achiever
        ]);
    }

    public function upgrade(Request $request)
    {
        $user = Auth::user();
        $u = User::find($user->id);
        $u->status = $request->status;
        $u->save();

        return response()->json([
            'info' => $u,
            'message' => 'success',
        ]);
    }
}
