<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Code;
use App\Models\Downline;
use App\Models\Purchase;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    public function register(Request $request) {
        $validatedData = $request->validate([
            'firstname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            // 'password' => ['required', 'string', 'confirmed'],
            'surname' => ['required', 'string'],
            'midname' => ['required', 'string'],
            'address' => ['required'],
            'gender' => ['required'],
            'civil' => ['required'],
            'birthmonth' => ['required'],
            'birthday' => ['required'],
            'birthyear' => ['required'],
            'cell' => ['required'],
            'username' => ['required', 'unique:users'],
            'sponsor' => ['required'],
            'sponsor_id' => ['required'],
            'position' => ['required'],
            'code' => ['required'],
            'pos' => ['required'],
        ]);
        $code = Code::where([['code', $request->code]])->first();

        if(!preg_match('/[A-Z]{3}[a-z]{4}[0-9]{4}[a-z]{2}/', $request->code) && 
            !preg_match('/[a-z]{3}[A-Z]{4}[0-9]{4}[a-z]{2}/', $request->code) && 
            !preg_match('/[A-Z]{3}[0-9]{4}[a-z]{4}[0-9]{2}/', $request->code) &&
            !preg_match('/[a-z]{3}[0-9]{4}[A-Z]{4}[a-z]{2}/', $request->code)){
            return response()->json([
                'message' => 'Code not valid',
                'code' => !preg_match('/[A-Z]{3}[a-z]{4}[0-9]{4}[a-z]{2}/', $request->code),
                'code2' => !preg_match('/[a-z]{3}[A-Z]{4}[0-9]{4}[a-z]{2}/', $request->code),
            ], 422);
        }

        if(!$code){
        return response()->json([
            'message' => 'Code does not exists',
        ], 422);
    }

        if($code){
            if($code->status == "Used"){
                return response()->json([
                    'message' => 'Code already used',
                ], 422);
            }
            
        }
        
        
        $validatedData['password'] = bcrypt($request->password);

        $user = new User();
        $user->firstname = $validatedData['firstname'];
        $user->email = $validatedData['email'];
        $user->password = $validatedData['password'];
        // if($request->admin){
        //     $user->admin = $request->admin;
        // }
        // if(preg_match('/[A-Z]{3}[a-z]{4}[0-9]{4}[a-z]{2}/', $request->code)){
        //     $user->type = 'Gold';
        // }else{
        //     $user->type = 'Pre-Membership';
        // }
        $user->address = $request->address;
        $user->surname = $request->surname;
        $user->midname = $request->midname;
        $user->gender = $request->gender;
        $user->civil = $request->civil;
        $user->birthmonth = $request->birthmonth;
        $user->birthday = $request->birthday;
        $user->birthyear = $request->birthyear;
        $user->cell = $request->cell;
        $user->username = $request->username;
        $user->code = $request->code;
        if($code){
            if($code->type == "Gold"){
                $user->isGold = 1;
                $user->status = "Gold";
            }elseif ($code->type == "Free Code") {
                $user->status = "Free";
            }elseif ($code->type == "CD Code") {
                $user->status = "CD";
            }else{
                $user->status = "Pre-Membership";
            }
            $code->status = 'Used';
            $code->save();
        }
        $user->sponsor = $request->sponsor;
        $user->position = $request->pos;
        $user->save();
        $id_number = +$user->id - 1;
        $prefix = $code->prefix;
        $user->id_number = $prefix.'-11337-'.$id_number;
        $user->save();

        

        $downline = new Downline();
        $downline->user_id = $user->id;
        $downline->sponsor_id = $request->sponsor_id;
        $downline->position = $request->position;

        $downline->save();

        //recursively save all uplines
        $spnsr = 2;
        $up = User::with('uplineRecursive')->find($user->id);
        $this->recursive(collect($up), $spnsr, $up->id, $up->created_at);

        $purchase = new Purchase();
        $purchase->user_id = $user->id;
        $purchase->set_id = 1;

        $purchase->save();

        $accessToken = $user->createToken('authToken')->accessToken;

        return response()->json([
            'info' => $downline,
            'user' => $user,
            'access_token' => $accessToken
        ], 201);
    }

    public function recursive($up, $id, $user, $date)
    {
        if(!isset($up['sponsor_id'])){
            return $this->recursive($up['upline_recursive'], $id, $user, $date);
        }else{
            if($up['sponsor_id']){
                DB::table('user_user')->insert([
                    'user_id' => $user,
                    'parent_id' => $up['sponsor_id'],
                    'position' => $up['position'],
                    'created_at' => Carbon::parse($date)->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::parse($date)->format('Y-m-d H:i:s')
                ]);  
                if($up['sponsor_id'] != $id){
                    return $this->recursive($up['upline_recursive'], $id, $user, $date);
                }else{
                    return $up;
                }
            }
        }
        
    }

    public function login(Request $request) {

        $loginData = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
        
        if (!auth()->attempt($loginData)) {
            return response()->json(['message' => 'E-mail or password is incorrect.'], 403);
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;

       
        return response()->json([
            'user' => auth()->user(),
            'admin' => auth()->user()->admin,
            'access_token' => $accessToken
        ]);
    }

    public function logout(Request $res)
    {
      if (Auth::user()) {

        $user = Auth::user()->token();
        $user->revoke();

        return response()->json([
          'success' => true,
          'message' => 'You are now logged out.'
          
      ]);

      } else {

        return response()->json([
          'success' => false,
          'message' => 'Cannot log out.'
        ]);

      }

     }
}
