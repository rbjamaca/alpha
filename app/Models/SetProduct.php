<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SetProduct extends Model
{
    use HasFactory;

    public function set()
    {
        return $this->belongsTo(Set::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
