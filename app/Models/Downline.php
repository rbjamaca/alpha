<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Downline extends Model
{
    use HasFactory;

    public function upline()
    {
        return $this->belongsTo(User::class, 'sponsor_id');
    }

    public function uplineRecursive()
    {
        return $this->upline()->with('uplineRecursive');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
