<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use \Staudenmeir\EloquentHasManyDeep\HasRelationships;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, HasFactory, HasRelationships;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function encashments()
    {
        return $this->hasMany(Encashment::class);
    }

    public function upline()
    {
        return $this->hasOne(Downline::class, 'user_id', 'id');
    }

    public function uplineRecursive()
    {
        return $this->upline()->with('uplineRecursive');
    }

    public function parent_dr()
    {
        return $this->hasOne(User::class, 'id_number', 'sponsor');
    }

    public function df()
    {
        return $this->hasMany(User::class, 'sponsor', 'id_number');
    }

    public function nested_dr()
    {
        return $this->df()->with('nested_dr');
    }

    public function downlines()
    {
        return $this->hasMany(Downline::class, 'sponsor_id');
    }

    public function purchases()
    {
        return $this->hasMany(Purchase::class);
    }

    public function code()
    {
        return $this->hasOne(Code::class, 'code', 'code');
    }

    public function codes()
    {
        return $this->hasOne(Code::class, 'code', 'code');
    }

    public function unilevel()
    {
        // return $this->hasmanyDeep(Purchase::class, ['user_user', User::class], ['parent_id', 'id', 'user_id'], ['id', 'user_id', 'id'])->withPivot('users')->select(['users.position', 'purchases.user_id', 'users.isGold', 'users.status']);
        return $this->belongsToMany(User::class, 'user_user', 'parent_id', 'user_id')->withPivot('position')->withCount('purchases as purch_count');
    }

    public function children()
    {
        return $this->belongsToMany(User::class, 'user_user', 'parent_id', 'user_id')->withPivot('position');
    }

    public function parent()
    {
        return $this->belongsToMany(User::class, 'user_user', 'user_id', 'parent_id')->withPivot('position');
    }

    public function nestedDownlines()
    {
        return $this->downlines()->with('user', function($q){
            return [$q->count(), $q->with(['nestedDownlines', 'purchases.set', 'code'])->withCount(['children as cd_left' => function($q){
                return $q->where('user_user.position', 'L')->whereHas('code', function($q){
                    $q->where('prefix', 'CD');
                });
            }, 'children as cd_right' => function($q){
                return $q->where('user_user.position', 'R')->whereHas('code', function($q){
                    $q->where('prefix', 'CD');
                });
            }, 'children as fr_left' => function($q){
                return $q->where('user_user.position', 'L')->whereHas('code', function($q){
                    $q->where('prefix', 'FR');
                });
            }, 'children as fr_right' => function($q){
                return $q->where('user_user.position', 'R')->whereHas('code', function($q){
                    $q->where('prefix', 'FR');
                });
            }, 'children as gl_left' => function($q){
                return $q->where('user_user.position', 'L')->whereHas('code', function($q){
                    $q->where('prefix', 'GL');
                });
            }, 'children as gl_right' => function($q){
                return $q->where('user_user.position', 'R')->whereHas('code', function($q){
                    $q->where('prefix', 'GL');
                });
            }, 'children as pr_left' => function($q){
                return $q->where('user_user.position', 'L')->whereHas('code', function($q){
                    $q->where('prefix', 'PR');
                });
            }, 'children as pr_right' => function($q){
                return $q->where('user_user.position', 'R')->whereHas('code', function($q){
                    $q->where('prefix', 'PR');
                });
            }, 'children as total'])];
        });
    }
}
