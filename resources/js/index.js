import React from 'react'
import ReactDOM from 'react-dom'
import {
  Route,
  BrowserRouter,
  Redirect,
  Switch,
  useHistory
} from 'react-router-dom'

import { SnackbarProvider } from 'notistack';
import { Provider } from './state'
import { MuiThemeProvider, createMuiTheme, withStyles } from '@material-ui/core/styles';
import { CssBaseline } from '@material-ui/core';
import LandingPage from './pages/LandingPage';
import Header from './layout/Header';
import ContactText from './components/ContactText';
import LoginPage from './pages/Auth/Login/LoginPage';
import { red } from '@material-ui/core/colors';
import UserDashboard from './pages/User/UserDashboard';
import MemberStructure from './pages/User/genealogy/MemberStructure';
import AdminDashboard from './pages/Admin/AdminDashboard';
import Generate from './pages/Admin/Generate';
import PurchaseForm from './pages/Admin/PurchaseForm';
import DashboardContent from './pages/User/DashboardContent';
import GoldRequests from './pages/Admin/GoldRequests';
import MultiGenerate from './pages/Admin/MultiGenerate';
import UnilevelRewards from './pages/User/rewards/UnilvelRewards';
import ReferralRewards from './pages/User/rewards/ReferralRewards';
import Members from './pages/User/genealogy/Members';
import Sponsored from './pages/User/genealogy/Sponsored';
import OrganizerRewards from './pages/User/rewards/OrganizerRewards';

const theme = createMuiTheme({
    overrides: {
      MuiListItem:{
        button: {
          '&:hover': {
            backgroundColor: 'none',
          }
        },
      },
      MuiIconButton: {
        root: {
          '&:focus': {
            outline: 'none',
          }
        },
      },
      MuiButton: {
        root: {
          '&:focus': {
            outline: 'none',
          },
          borderRadius: 0,
        },
      },
      MuiButtonBase: {
        root: {
          '&:focus': {
            outline: 'none',
          },
          borderRadius: 0,
        },
      },
      TextField: {
        '& fieldset': {
          borderRadius: 0,
        },
    },
    MuiOutlinedInput: {
         
      root: {
        "& $notchedOutline": {
          borderColor: '##a59c9c',
          borderWidth: 1,
        },
        "&:hover $notchedOutline": {
          borderColor: '#E0E0E0',
          borderWidth: 1,
        },
        "&$focused $notchedOutline": {
          borderColor: '#E0E0E0',
          borderWidth: 1,
        },
        // [`& fieldset`]: {
        //             borderRadius: '10em',
        //         },
      }
    },
    MuiSelect: {
      select: {  
        '&:focus': {
          background: 'none',
          backgroundColor: 'none'
        }
      }
    },
  },
    palette: {
      header: {
        main: '#fff',
        contrastText: '#243442',
      },
      background: {
        default: "#fff"
      },
      primary: {
        light: '#b3e0e5',
        main: '#007a8c',
        dark: '#007a8c',
        contrastText: '#fff',
      },
      secondary: {
        light: red[300],
        main: red[500],
        dark: red[900],
        contrastText: '#fff',
      },
      pink: {
        main: '#f33950',
      }
    },
    '@global': {
      '*::-webkit-scrollbar': {
        display: 'none',
      },
      '*': {
        '-ms-overflow-style': 'none',
        'scrollbar-width': 'none',
      }
    },
    // typography: {
    //   fontFamily: [
    //     '-apple-system',
    //     'BlinkMacSystemFont',
    //     '"Calibri"',
    //   ].join(','),
    // },
  });

const AdminRoute = ({exact, path, component: Component}) => (
<Route exact={exact} path={path} render={(props) => (
    <div>
    <AdminDashboard {...props}>
      <Component {...props}/>
    </AdminDashboard>
    </div>
)}/>
);

const HeaderRoute = ({exact, path, component: Component}) => (
  <Route exact={exact} path={path} render={(props) => (
      <div>
      <Header {...props}/>
      <Component {...props}/>
      </div>
  )}/>
  );

const DashRoute = ({exact, path, component: Component}) => (
  <Route exact={exact} path={path} render={(props) => (
      <div>
      <UserDashboard {...props}>
      <Component {...props}/>
      </UserDashboard>
      </div>
  )}/>
  );

const App = () => {
  return (
    <BrowserRouter>
      <div>
        <Provider>
          <Switch>
            <HeaderRoute exact path="/" component={LandingPage} />
            <HeaderRoute exact path="/login" component={LoginPage} />
            <DashRoute exact path="/user/genealogy/structure" component={MemberStructure} />
            <DashRoute exact path="/user/genealogy/members" component={Members} />
            <DashRoute exact path="/user/genealogy/sponsored" component={Sponsored} />
            <DashRoute exact path="/user/dashboard" component={DashboardContent} />
            <DashRoute exact path="/user/rewards/unilevel" component={UnilevelRewards} />
            <DashRoute exact path="/user/rewards/direct-referral" component={ReferralRewards} />
            <DashRoute exact path="/user/rewards/matching" component={ReferralRewards} />
            <DashRoute exact path="/user/rewards/achiever-rewards" component={OrganizerRewards} />
            <DashRoute exact path="/user/rewards/organizer-rewards" component={OrganizerRewards} />
            <AdminRoute exact path="/admin/generate-code" component={Generate} />
            <AdminRoute exact path="/admin/multi-code" component={MultiGenerate} />
            <AdminRoute exact path="/admin/purchases" component={PurchaseForm} />
            <AdminRoute exact path="/admin/gold" component={GoldRequests} />
            <Route render={() => <h1>404: page not found</h1>} />
          </Switch>
        </Provider>
      </div>
    </BrowserRouter>
  )
}

ReactDOM.render(
  <MuiThemeProvider theme={theme}>
    <SnackbarProvider maxSnack={3}
    disableWindowBlurListener={true}
    preventDuplicate
    anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'right',
    }}>
      <CssBaseline />
      <App />
    </SnackbarProvider>
  </MuiThemeProvider>,
  document.getElementById('app')
)
