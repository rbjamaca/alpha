import { Typography } from '@material-ui/core';
import React from 'react';

function ContactText() {
    return (
            <Typography variant="body1">
                Zone 3, Brgy.23 Julio Pacana St. Licoan, Cagayan de Oro City  contact: 09353308065
            </Typography>
    );
}

export default ContactText;