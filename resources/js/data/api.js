import axios from 'axios';
import { isLoggedIn } from '../auth';

export const login = async ( userObject ) => {
    return await axios.post("/api/auth/login", userObject, {
                        }).then(response => {
                        return response;
      });
}

export const API_ACTION = async (data, url, id = null) => {
    let method = id == null ? axios.post : axios.put
    return await method(url, data, {
        headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + isLoggedIn()
        }
    }).then(response => {
        return response;
    });
}

export const API_RETRIEVE = async (url, action = '') => {
    let method = action == '' ? axios.get : axios.delete
    return await method(url, {
        headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + isLoggedIn()
        }
    }).then(response => {
        return response;
    });
}

export const register = async ( userObject ) => {
    return await axios.post("/api/auth/register", userObject, {
                        }).then(response => {
                        return response;
      });
}

export const fetchUser = async () => {
    return await axios.get("/api/user/", {
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + isLoggedIn()
          }
                        }).then(response => {
                        return response;
      });
}

export const fetchUserId = async (id) => {
    return await axios.get("/api/user/"+id, {
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + isLoggedIn()
          }
                        }).then(response => {
                        return response;
      });
}

export const fetchCode = async () => {
    return await axios.get("/api/admin/code", {
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + isLoggedIn()
          }
                        }).then(response => {
                        return response;
      });
}

export const saveCode = async (Obj) => {
    return await axios.post("/api/admin/code", Obj, {
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + isLoggedIn()
          }
                        }).then(response => {
                        return response;
      });
}

export const saveCodes = async (Obj) => {
    return await axios.post("/api/admin/codes", Obj, {
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + isLoggedIn()
          }
                        }).then(response => {
                        return response;
      });
}

export const fetchPurchases = async () => {
    return await axios.get("/api/admin/purchases", {
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + isLoggedIn()
          }
                        }).then(response => {
                        return response;
      });
}


export const savePurchase = async (Obj) => {
    return await axios.post("/api/admin/purchases", Obj, {
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + isLoggedIn()
          }
                        }).then(response => {
                        return response;
      });
}


export const searchById = async (id) => {
    return await axios.get("/api/admin/search-by-id/"+id, {
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + isLoggedIn()
          }
                        }).then(response => {
                        return response;
      });
}

export const userSearchById = async (id, up) => {
    return await axios.get("/api/user/search-by-id/"+id+"/"+up, {
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + isLoggedIn()
          }
                        }).then(response => {
                        return response;
      });
}

export const updateCode = async (Obj, id) => {
    return await axios.put("/api/admin/code/"+id, Obj, {
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + isLoggedIn()
          }
                        }).then(response => {
                        return response;
      });
}

export const fetchRequests = async () => {
    return await axios.get("/api/admin/gold/requests", {
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + isLoggedIn()
          }
                        }).then(response => {
                        return response;
      });
}

export const upgradeGold = async (Obj, id) => {
    return await axios.put("/api/admin/gold/"+id, Obj, {
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + isLoggedIn()
          }
                        }).then(response => {
                        return response;
      });
}

export const fetchDownlines = async (id) => {
    return await axios.get("/api/user/downlines/"+id, {
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + isLoggedIn()
          }
                        }).then(response => {
                        return response;
      });
}

export const fetchMetrics = async () => {
    return await axios.get("/api/user/dash/metrics", {
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + isLoggedIn()
          }
                        }).then(response => {
                        return response;
      });
}

export const memberUpgrade = async (Obj) => {
    return await axios.post("/api/user/membership/upgrade", Obj, {
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + isLoggedIn()
          }
                        }).then(response => {
                        return response;
      });
}

