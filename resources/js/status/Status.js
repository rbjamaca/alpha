import _ from '../@lodash';
import clsx from 'clsx';
import React from 'react';
import { Chip, makeStyles } from '@material-ui/core';
import { blue, deepPurple, green, orange, yellow } from '@material-ui/core/colors';

const useStyles = makeStyles((theme) => ({
    blue: {
		backgroundColor: blue[500],
	},
	green: {
		backgroundColor: green[500],
	},
	orange: {
		backgroundColor: orange[500],
	},
	yellow: {
		backgroundColor: yellow[500],
	},
	purple: {
		backgroundColor: deepPurple[500],
	},
}));

function Status(props) {
	const classes = useStyles();

	const orderStatuses = [
		{
			id: 1,
			name: 'Generated',
			color: classes.blue
		},
		{
			id: 2,
			name: 'Used',
			color: classes.green
		},
		{
			id: 3,
			name: 'Assigned',
			color: classes.orange
		},
		{
			id: 4,
			name: 'Sent',
			color: 'bg-purple text-white'
		},
		{
			id: 5,
			name: 'Delivered',
			color: 'bg-green-700 text-white'
		},
		{
			id: 6,
			name: 'Canceled',
			color: 'bg-pink text-white'
		},
		{
			id: 7,
			name: 'Refunded',
			color: 'bg-red text-white'
		},
		{
			id: 8,
			name: 'Pending for Gold',
			color: classes.orange
		},
		{
			id: 9,
			name: 'On pre-order (paid)',
			color: 'bg-purple-300 text-white'
		},
		{
			id: 10,
			name: 'Awaiting bank wire payment',
			color: 'bg-blue text-white'
		},
		{
			id: 11,
			name: 'Awaiting PayPal payment',
			color: 'bg-blue-700 text-white'
		},
		{
			id: 12,
			name: 'Remote payment accepted',
			color: 'bg-green-800 text-white'
		},
		{
			id: 13,
			name: 'On pre-order (not paid)',
			color: 'bg-purple-700 text-white'
		},
		{
			id: 14,
			name: 'Gold',
			color: clsx( classes.yellow, 'text-white')
		},
		{
			id: 15,
			name: 'Pre-Membership',
			color: clsx( classes.green, 'text-white')
		},
		{
			id: 16,
			name: 'CD',
			color: clsx( classes.orange, 'text-white')
		},
		{
			id: 17,
			name: 'Free',
			color: clsx( classes.purple, 'text-white')
		},
		{
			id: 18,
			name: 'Awaiting Cash-on-delivery payment',
			color: 'bg-blue-800 text-white'
		},
	];
	return (
		<Chip className={clsx('text-white', _.find(orderStatuses, { name: props.name }).color)} label={props.name} />
	);
}

export default Status;
