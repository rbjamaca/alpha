export const setToken = (token) => localStorage.setItem('token', token)
export const isLoggedIn = () => localStorage.getItem('token')
export const setAdmin = (admin) => localStorage.setItem('isAdmin', admin)
export const isAdmin = () => localStorage.getItem('isAdmin')
export const logout = callback => {
    setToken('')
    window.location = '/'
    callback()
  }