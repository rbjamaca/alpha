import React, { useState, useEffect, useRef } from 'react'

//material-ui
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { green, orange, red } from '@material-ui/core/colors';
import { Avatar, Box, Hidden } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  header: {
    backgroundColor: red[900],
  },
  input: {
    padding: 1,
    borderRadius: '10em',
    fontSize: '1em',
  },
  backDrop: {
    backdropFilter: "blur(3px)",
    backgroundColor: 'rgba(0,0,30,0.4)'
  },
  list: {
    width: 300,
  },
  title: {
    flexGrow: 1,
    color: 'white',
  },
  subtitle: {
    color: '#3e7e96',
  },
  tab: {
    '&:focus': {
      outline: 'none',
    }
  },
  active: {
    color: orange[700],
  },
  iconButton: {
    '&:focus': {
      outline: 'none',
    },
    color: '#3e7e96'
  },
  large: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
}));


const Header = (props) => {
    const classes = useStyles();
    const sections = [
        {title: "HOME", url: "/"},
        {title: "PRODUCTS", url: "/products"},
        {title: "CONTACT US", url: "/contact-us"},
        {title: "ACTIVITIES", url: "/activities"},
        {title: "LOGIN", url: "/login"},
    ]

    const mobile = [
      {title: "LOGIN", url: "/login"},
  ]

    const goRedirect = (section) => {
        props.history.push(section.url)
    }
    
    return (
    <div className={classes.root}>
        <AppBar className={classes.header} position="fixed">
        <Toolbar>
            <Typography variant="h4" className={classes.title}>
                <IconButton onClick={() => props.history.push('/')}>
                <Avatar src="/images/image3.png" alt="logo" variant="circular" className={classes.large} />
                </IconButton>
                <Hidden mdDown>
                  <a href="/" style={{ textDecoration: 'none', color: 'inherit' }}>
                  <b>{"ALPHA & MEGA"}</b>
                  </a>
                </Hidden>
            </Typography>
            <Hidden mdDown>
            {sections.map((section, i) => (
                <Box key={i} ml={2}>
                    <Button variant="text" color="inherit" className={window.location.href === window.location.origin+section.url ? classes.active : ""} onClick={()=> goRedirect(section)}>
                        <Typography variant="h6" className={classes.tab}>
                            {section.title}
                        </Typography>
                    </Button>
                </Box>
                
            ))}
            </Hidden>
            <Hidden lgUp>
            {mobile.map((section, i) => (
                <Box key={i} ml={2}>
                    <Button variant="text" color="inherit" className={window.location.href === window.location.origin+section.url ? classes.active : ""} onClick={()=> goRedirect(section)}>
                        <Typography variant="h6" className={classes.tab}>
                            {section.title}
                        </Typography>
                    </Button>
                </Box>
                
            ))}
            </Hidden>
            </Toolbar>
        </AppBar>
    </div>
    )
}

export default Header;
