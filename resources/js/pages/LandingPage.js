import { Box, Grid, makeStyles, Typography } from '@material-ui/core'
import { red } from '@material-ui/core/colors';
import React, { useEffect, useRef, useState } from 'react'
import { useHistory } from 'react-router';
import { isAdmin, isLoggedIn } from '../auth';
import ContactText from '../components/ContactText';

const useStyles = makeStyles((theme) => ({
    root: {
        height: '80vh',
        flexGrow: 1,
    },
    image: {
        backgroundImage: `url(${window.location.origin}/images/newbg.png)`,
        backgroundRepeat: 'no-repeat',
        backgroundColor: theme.palette.common.white,
        // backgroundPosition: 'center',
        backgroundSize: 'cover',
        borderRadius: "50%",
        width: '100%',
        height: 'auto',
        flexGrow: 1,
    },
    quote : {
        color: red[900],
    },
    author : {
        color: theme.palette.grey,
    },
}))


const LandingPage = (props) => {
    const classes = useStyles();
    const history = useHistory();

        useEffect(() => {
            if(isLoggedIn()){
                if(isAdmin() === "admin"){
                    history.push('/admin/generate-code')
                }else{
                    history.push('/user/genealogy/structure')
                }
            }

        }, [])
    return (
        <>
        <Box pt={10}>
        </Box>
        <ContactText/>
        <Box pt={7}>
        </Box>
        <Grid container className={classes.root} justify="center">
            
            
            <Grid item xs md={10} className={classes.image}>
                <Grid direction="row" container justify="flex-start" alignItems="flex-end" style={{flexGrow:1, height: "65vh"}}>
                    <Grid item>
                        <Typography variant="h3" className={classes.quote} >
                        “DREAM IN AN OPEN EYES FOR SUCCESS“
                        </Typography>
                        <Typography variant="h5" className={classes.author} align="right" >
                        <i>Masterbuyo</i>
                        </Typography>
                    </Grid>
                </Grid>

            </Grid>

            
            
        </Grid>
        </>
    )
}

export default LandingPage