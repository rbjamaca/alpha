import { Button, Divider, Grid, makeStyles, Paper, TextField, Typography } from '@material-ui/core';
import { red } from '@material-ui/core/colors';
import React, { useState, useEffect } from 'react'
import {fetchCode, fetchRequests} from '../../data/api'
import CodeTable from './CodeTable'
import GoldReqTable from './GoldReqTable';

const useStyles = makeStyles((theme) => ({
    title: {
        color: red[700],
    },
    regheader: {
        backgroundColor: red[800],
    },
    regtitle: {
        color: theme.palette.common.white,
        margin: theme.spacing(1),
    },
    form: {
        paddingLeft: theme.spacing(4),
        paddingRight: theme.spacing(4),
    },
    formControl: {
        minWidth: 120,
    },
    large: {
        width: theme.spacing(7),
        height: theme.spacing(7),
    },
    userIcon: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
        width: theme.spacing(8),
        height: theme.spacing(8),
    },
    fullWidth: {
        margin: theme.spacing(4),
        flexGrow: 1,
    },
}));

const GoldRequests = (props) => {
    const classes = useStyles();
    const [code, setCode] = useState("")
    const [preregCode, setpreregCode] = useState("")
    const [update, setUpdate] = useState(false);

    const [rows, setRows] = useState([])
    const getRequests = () => {
        fetchRequests().then(res => {
            setRows(res.data.list)
        })
    }
    

    useEffect(() => {
        getRequests();
    }, [update])
    

    return (
        <div className={classes.fullWidth}>
            <Grid container>
                <Grid item xs={12}>
                    <Paper style={{height:"80vh"}}>
                        <GoldReqTable update={update} setUpdate={setUpdate} rows={rows}/>
                    </Paper>
                </Grid>
            </Grid>
          
            
        </div>
    )
}

export default GoldRequests
