import { Button, Divider, FormControl, Grid, InputLabel, makeStyles, NativeSelect, Paper, TextField, Typography, withStyles } from '@material-ui/core';
import { red } from '@material-ui/core/colors';
import React, { useState, useEffect } from 'react'
import {fetchPurchases, saveCode, savePurchase, searchById} from '../../data/api'
import useForm from '../../data/useForm';
import InputBase from '@material-ui/core/InputBase';
import PurchasesTable from './PurchasesTable'
import { useSnackbar } from 'notistack';

const BootstrapInput = withStyles((theme) => ({
    root: {
        'label + &': {
            marginTop: 7,
        },
    },
    input: {
        borderRadius: 4,
        position: 'relative',
        backgroundColor: theme.palette.background.paper,
        border: '1px solid #ced4da',
        fontSize: 16,
        padding: '10px 26px 10px 12px',
        transition: theme.transitions.create(['border-color', 'box-shadow']),
        // Use the system font instead of the default Roboto font.
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
        '&:focus': {
            borderRadius: 4,
            borderColor: '#80bdff',
            boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
        },
    },
}))(InputBase);

const useStyles = makeStyles((theme) => ({
    title: {
        color: red[700],
    },
    regheader: {
        backgroundColor: red[800],
    },
    regtitle: {
        color: theme.palette.common.white,
        margin: theme.spacing(1),
    },
    form: {
        paddingLeft: theme.spacing(4),
        paddingRight: theme.spacing(4),
    },
    formControl: {
        minWidth: 210,
    },
    large: {
        width: theme.spacing(7),
        height: theme.spacing(7),
    },
    userIcon: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
        width: theme.spacing(8),
        height: theme.spacing(8),
    },
    fullWidth: {
        margin: theme.spacing(4),
        flexGrow: 1,
    },
}));

const PurchaseForm = (props) => {
    const classes = useStyles();
    const [code, setCode] = useState("")
    const [preregCode, setpreregCode] = useState("")
    const [update, setUpdate] = useState(false);

    const { form, handleChange, resetForm } = useForm({
       id_number: '',
       set: '',
    });

    const [rows, setRows] = useState([])
    const [sets, setSets] = useState([])
    const [user, setUser] = useState(null)
    const getCodes = async () => {
        await fetchPurchases().then(res => {
            setRows(res.data.list)
            setSets(res.data.sets)
        })
    }

    const search = () => {
        searchById(form.id_number).then(res => {
            setUser(res.data.info);
        })
    }

    const { enqueueSnackbar, closeSnackbar } = useSnackbar();


    const savePurch = (e) => {
        e.preventDefault()
        if(user){
            form.id = user.id;
            savePurchase(form).then(res => {
                console.log(res)
                resetForm()
                setUser(null)
                setUpdate(!update)
            })
        }else{
            if(form.id_number){
                enqueueSnackbar('Verify ID Number field', {
                    variant: 'error',
                })
            }else{
                enqueueSnackbar('Member not found', {
                    variant: 'error',
                })
            }
        }
    }
    
    
    

    useEffect(() => {
        getCodes();
    }, [update])
    

    return (
        <div className={classes.fullWidth}>
            <form onSubmit={savePurch} >
            <Grid container>
                <Grid item xs={4}>
                <Grid container direction="column" alignItems="flex-start" spacing={2}>
                <Paper style={{padding:10, height:"30vh", width:"100%"}}>
                    <Typography variant="h6">
                        MEMBER PURCHASE FORM
                    </Typography>
                    <Grid item xs={12}>
                    <Grid container spacing={1}>
                        <Grid item>
                            <TextField
                                margin="dense"
                                id="id_number"
                                variant="outlined"
                                label="ID Number"
                                name="id_number"
                                value={form.id_number}
                                onChange={handleChange}
                                type="text"
                                required
                            />
                        </Grid>
                        <Grid item>
                            <Button style={{ marginTop: 10 }} onClick={search} size="medium" variant="contained" color="primary">
                                Verify
                            </Button>
                        </Grid>
                    </Grid>
                    </Grid>
                    {user && 
                    <Typography style={{padding:7, backgroundColor: "#b9b9b9", maxWidth: 210}}  variant="body1">
                        {user.firstname} {user.surname}    
                    </Typography>}

                    <Grid item xs={12}>
                    <FormControl variant="outlined" className={classes.formControl}>
                                <InputLabel>Set</InputLabel>
                                <NativeSelect
                                    name="set"
                                    value={form.set}
                                    onChange={handleChange}
                                    // onChange={(e) => {}}
                                    label="Set"
                                    input={<BootstrapInput />}
                                    required
                                >
                                    <option value="">
                                    </option>
                                    {sets.map(_ => <option key={_.id} value={_.id}>{_.desc}</option>)}
                                </NativeSelect>
                            </FormControl>
                    </Grid>
                    <Grid item>
                    <Button variant="outlined" type="submit" color="primary" style={{marginTop:5}}>
                        Save
                    </Button>
                    </Grid>
                    </Paper>  
                </Grid>
                </Grid>
                <Grid item xs={8}>
                    <Paper style={{height:"75vh"}}>
                        <PurchasesTable rows={rows}/>
                    </Paper>
                </Grid>
            </Grid>
            </form>
          
            
        </div>
    )
}

export default PurchaseForm
