import React, { useEffect, useState } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import Box from '@material-ui/core/Box';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Link from '@material-ui/core/Link';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import NotificationsIcon from '@material-ui/icons/Notifications';
import { red } from '@material-ui/core/colors';
import { Avatar, Button, Collapse } from '@material-ui/core';

// icons
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import DashboardIcon from '@material-ui/icons/Dashboard';
import PeopleIcon from '@material-ui/icons/People';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import CreditCardIcon from '@material-ui/icons/CreditCard';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { fetchUser } from '../../data/api';
import Moment from 'react-moment';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import CasinoIcon from '@material-ui/icons/Casino';
import { isAdmin, isLoggedIn, logout } from '../../auth';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href={window.location.origin}>
        {"ALPHA & MEGA"}
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
    backgroundColor: red[900],
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    height: "80px",
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
    color: 'white',
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
  large: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
  logo: {
    width: theme.spacing(7),
    height: theme.spacing(7),
    marginRight: 110,
  },
  userIcon: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    width: theme.spacing(8),
    height: theme.spacing(8),
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
}));

export default function AdminDashboard(props) {

    const classes = useStyles();
    const [open, setOpen] = React.useState(true);
    const [user, setUser] = useState(null);
    const [geopen, setGeopen] = useState(false)
    const [reopen, setReopen] = useState(false)
    const url = window.location.href;
    const origin = window.location.origin+'/admin/';
    const handleDrawerOpen = () => {
    setOpen(true);
    };
    const handleDrawerClose = () => {
    setOpen(false);
    };
    const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
    const getUser = async () => {
        fetchUser().then(res => {
            setUser(res.data.user)
        })
    }

    const sections = [
        {title: "HOME", url: "/"},
        {title: "PRODUCTS", url: "/products"},
        {title: "CONTACT US", url: "/contact-us"},
        {title: "ACTIVITIES", url: "/activities"},
    ]

    const clickGenealogy = () => {
        setGeopen(!geopen);
    }

    const clickRewards = () => {
        setReopen(!reopen);
    }
    

  
    useEffect(() => {
      if(!isLoggedIn() || !isAdmin() === "admin"){
        window.location = '/'
      }
        getUser()
    }, []);

    const mainListItems = (
    <div>
        <ListItem button>
        <ListItemIcon>
            <DashboardIcon />
        </ListItemIcon>
        <ListItemText disabled primary="DASHBOARD" />
        </ListItem>
        <ListItem button selected={url === origin+'generate-code'} onClick={()=> props.history.push('/admin/generate-code')}>
        <ListItemIcon>
            <CasinoIcon />
        </ListItemIcon>
        <ListItemText primary="GENERATE CODE" />
        </ListItem>
        <ListItem button selected={url === origin+'multi-code'} onClick={()=> props.history.push('/admin/multi-code')}>
        <ListItemIcon>
            <CasinoIcon />
        </ListItemIcon>
        <ListItemText primary="BATCH CODES" />
        </ListItem>
        <ListItem button selected={url === origin+'gold'} onClick={()=> props.history.push('/admin/gold')}>
        <ListItemIcon>
            <CasinoIcon />
        </ListItemIcon>
        <ListItemText primary="GOLD REQUESTS" />
        </ListItem>
        <ListItem button selected={url === origin+'purchases'} onClick={()=> props.history.push('/admin/purchases')}>
        <ListItemIcon>
            <CasinoIcon />
        </ListItemIcon>
        <ListItemText primary="REPURCHASE" />
        </ListItem>
        <ListItem button onClick={logout}>
        <ListItemIcon>
            <ExitToAppIcon />
        </ListItemIcon>
        <ListItemText primary="LOGOUT" />
        </ListItem>
    </div>
    );

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="absolute" className={clsx(classes.appBar, open && classes.appBarShift)}>
        <Toolbar className={classes.toolbar}>
          <IconButton
            edge="start"
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            className={clsx(classes.menuButton, open && classes.menuButtonHidden)}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h4" className={classes.title}>
                <IconButton onClick={() => props.history.push('/user/dashboard')}>
                <Avatar src="/images/image3.png" alt="logo" variant="circular" className={classes.large} />
                </IconButton>
                
                <a href="/" style={{ textDecoration: 'none', color: 'inherit' }}>
                <b>{"ALPHA & MEGA"}</b>
                </a>
            </Typography>
            {sections.map((section, i) => (
                <Box key={i} ml={2}>
                    <Button variant="text" color="inherit" className={window.location.href === window.location.origin+section.url ? classes.active : ""} onClick={()=> goRedirect(section)}>
                        <Typography variant="h6" className={classes.tab}>
                            {section.title}
                        </Typography>
                    </Button>
                </Box>
                
            ))}
          {/* <IconButton color="inherit">
            <Badge badgeContent={4} color="secondary">
              <NotificationsIcon />
            </Badge>
          </IconButton> */}
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        classes={{
          paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
        }}
        open={open}
      >
        <div className={classes.toolbarIcon}>
                <Avatar src="/images/image3.png" alt="logo" variant="circular" className={classes.logo} />
            <Typography variant="h4">
            </Typography>
            <IconButton onClick={handleDrawerClose}>
                <ChevronLeftIcon />
            </IconButton>
        </div>
        <Divider />
        <Grid container direction="column" alignItems="center" justify="center" style={{opacity: open ? 1 : 0}}>
            <Grid item>
                <Avatar variant="circular" src="/images/image7.png" className={classes.userIcon} /> 
                
            </Grid>
            <Grid item>
            {user && user.name}
            </Grid>
            <Grid item>
            {user && 
                <Moment format="LL">
                    {user.created_at}
                </Moment>
            }
            </Grid>
        </Grid>
        <List>{mainListItems}</List>
        <Divider />
      </Drawer>
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Container maxWidth="xl" className={classes.container}>
          <Grid container spacing={3}>
            {/* <Grid item xs={12} md={8} lg={9}>
              <Paper className={fixedHeightPaper}>
              </Paper>
            </Grid>
            <Grid item xs={12} md={4} lg={3}>
              <Paper className={fixedHeightPaper}>
              </Paper>
            </Grid>
            <Grid item xs={12}>
              <Paper className={classes.paper}>
              </Paper>
            </Grid>*/}
            {props.children}
          </Grid>
          <Box pt={4}>
            <Copyright />
          </Box>
        </Container>
      </main>
    </div>
  );
}