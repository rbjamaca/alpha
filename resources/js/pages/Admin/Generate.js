import { Button, Divider, Grid, makeStyles, Paper, TextField, Typography } from '@material-ui/core';
import { red } from '@material-ui/core/colors';
import React, { useState, useEffect } from 'react'
import {fetchCode, saveCode} from '../../data/api'
import CodeTable from './CodeTable'

const useStyles = makeStyles((theme) => ({
    title: {
        color: red[700],
    },
    regheader: {
        backgroundColor: red[800],
    },
    regtitle: {
        color: theme.palette.common.white,
        margin: theme.spacing(1),
    },
    form: {
        paddingLeft: theme.spacing(4),
        paddingRight: theme.spacing(4),
    },
    formControl: {
        minWidth: 120,
    },
    large: {
        width: theme.spacing(7),
        height: theme.spacing(7),
    },
    userIcon: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
        width: theme.spacing(8),
        height: theme.spacing(8),
    },
    fullWidth: {
        margin: theme.spacing(4),
        flexGrow: 1,
    },
}));

const Generate = (props) => {
    const classes = useStyles();
    const [code, setCode] = useState("")
    const [cdCode, setCdCode] = useState("")
    const [freeCode, setFreeCode] = useState("")
    const [preregCode, setpreregCode] = useState("")
    const [update, setUpdate] = useState(false);

    function makeCodeU(length) {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    function makeCodeL(length) {
        var result           = '';
        var characters       = 'abcdefghijklmnopqrstuvwxyz';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }
    function makeCodeN(length) {
        var result           = '';
        var characters       = '0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    const makeCode = () => {
        const code = makeCodeU(3)+makeCodeL(4)+makeCodeN(4)+makeCodeL(2);
        setCode(code);
        saveCode({code:code, type:"Gold", prefix: 'GL'}).then(_ => setUpdate(!update));
    }

    const makeCodePreReg = () => {
        const code = makeCodeL(3)+makeCodeU(4)+makeCodeN(4)+makeCodeL(2);
        setpreregCode(code);
        saveCode({code:code, type:"Pre-Membership", prefix: 'PR'}).then(_ => setUpdate(!update));
    }


    const makeCdCode = () => {
        const code = makeCodeU(3)+makeCodeN(4)+makeCodeL(4)+makeCodeN(2);
        setCdCode(code);
        saveCode({code:code, type:"CD Code", prefix: 'CD'}).then(_ => setUpdate(!update));
    }

    const makeFreeCode = () => {
        const code = makeCodeL(3)+makeCodeN(4)+makeCodeU(4)+makeCodeL(2);
        setFreeCode(code);
        saveCode({code:code, type:"Free Code", prefix: 'FR'}).then(_ => setUpdate(!update));
    }

    const [rows, setRows] = useState([])
    const getCodes = async () => {
        await fetchCode().then(res => {
            setRows(res.data.list)
        })
    }
    

    useEffect(() => {
        getCodes();
    }, [update])
    

    return (
        <div className={classes.fullWidth}>
            <Grid container>
                <Grid item xs={4}>
                <Grid container direction="column" alignItems="flex-start">
                <Paper style={{padding:10, height:"70vh"}}>
                    <Typography variant="h6">
                        Generate Codes
                    </Typography>
                    <Grid item xs={12}>
                    <TextField
                        margin="dense"
                        id="code"
                        value={preregCode}
                        variant="outlined"
                        label="Pre-Membership"
                        type="text"
                        fullWidth
                        inputProps={{
                            readOnly:true,
                        }}
                    />
                    </Grid>
                    <Grid item>
                    <Button variant="outlined" onClick={makeCodePreReg}>
                        Generate
                    </Button>
                    </Grid>

                    <Divider style={{margin:20}} />

                    <Grid item xs={12}>
                    <TextField
                        margin="dense"
                        id="gold"
                        value={code}
                        variant="outlined"
                        label="Gold Membership"
                        type="text"
                        fullWidth
                        inputProps={{
                            readOnly:true,
                        }}
                    />
                    </Grid>
                    <Grid item>
                    <Button variant="outlined" onClick={makeCode}>
                        Generate
                    </Button>
                    </Grid>
                    
                    <Divider style={{margin:20}} />

                    <Grid item xs={12}>
                    <TextField
                        margin="dense"
                        id="gold"
                        value={cdCode}
                        variant="outlined"
                        label="CD Code"
                        type="text"
                        fullWidth
                        inputProps={{
                            readOnly:true,
                        }}
                    />
                    </Grid>
                    <Grid item>
                    <Button variant="outlined" onClick={makeCdCode}>
                        Generate
                    </Button>
                    </Grid>
                    
                    <Divider style={{margin:20}} />

                    <Grid item xs={12}>
                    <TextField
                        margin="dense"
                        id="gold"
                        value={freeCode}
                        variant="outlined"
                        label="Free Code"
                        type="text"
                        fullWidth
                        inputProps={{
                            readOnly:true,
                        }}
                    />
                    </Grid>
                    <Grid item>
                    <Button variant="outlined" onClick={makeFreeCode}>
                        Generate
                    </Button>
                    </Grid>
                    </Paper>  
                </Grid>
                </Grid>
                <Grid item xs={8}>
                    <Paper style={{height:"80vh"}}>
                        <CodeTable update={update} setUpdate={setUpdate} rows={rows}/>
                    </Paper>
                </Grid>
            </Grid>
          
            
        </div>
    )
}

export default Generate
