import React from 'react'
// import Login from './Login'
import Grid from '@material-ui/core/Grid'
import { makeStyles, fade } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import { Typography } from '@material-ui/core'
import Login from './Login'

const useStyles = makeStyles((theme) => ({
    root: {
        height: '100vh'
    },
    image: {
        width: '100%',
        height: 'auto',
    },
    logo: {
        backgroundImage: `url(${window.location.origin}/images/image8.png)`,
        backgroundRepeat: 'no-repeat',
        backgroundColor: theme.palette.common.white,
        backgroundPosition: 'center',
        // backgroundSize: 'cover',
        width: '100%',
        height: 'auto',
        flexGrow: 1,
    },
}))

const LoginPage = () => {
    const classes = useStyles();
    return (
        <Grid container className={classes.root}>
            <Grid item xs md={8} className={classes.logo}/>             
            <Grid item xs={12} sm={12} md={4} component={Paper} elevation={6} square>
                <Grid container justify="center" alignItems="center" style={{height:"80%"}}>
                    <Grid item>
                        <Login />
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    )
}

export default LoginPage