import React, { useEffect, useState } from 'react'
// import Login from './Login'
import Grid from '@material-ui/core/Grid'
import { makeStyles, fade, createMuiTheme, MuiThemeProvider, ThemeProvider } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import { Avatar, Button, Box, Checkbox, FormControlLabel, TextField, Typography, CircularProgress } from '@material-ui/core'
import { green, red } from '@material-ui/core/colors'
import { useHistory } from 'react-router-dom'
import { login } from '../../../data/api'
import { isAdmin, isLoggedIn, setAdmin, setToken } from '../../../auth'

const useStyles = makeStyles((theme) => ({
    input: {
        padding: 1,
        marginBottom: 20,
    },
    error: {
        color: red[500],
        fontSize: 12,
        display: "block",
    },
    paper: {
        paddingTop: 2,
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    paper_reg: {
        margin: theme.spacing(3, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(5, 5, 0, 5),
        backgroundColor: theme.palette.secondary.dark,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        '&:hover': {
            backgroundColor: green[800],
        },
        margin: theme.spacing(1, 0, 2),
        backgroundColor: green[600],
    },
    radio: {
        '&$checked': {
          color: green[600],
        }
      },
    checked: {}
}));

const Login = () => {
        const classes = useStyles();
        const [email, setEmail] = useState("")
        const [password, setPassword] = useState("")
        const [loading, setLoading] = useState(false)
        const history = useHistory();

        useEffect(() => {
            if(isLoggedIn()){
                if(isAdmin() === 1){
                    history.push('/admin/generate-code')
                }else{
                    history.push('/user/dashboard')
                }
            }

        }, [])
        const theme = createMuiTheme({
            palette: {
              primary: { main: green[600] },
            },
        });
        // Error Detectors
        const [isError, setIsError] = useState(false)
        const [accError, setAccError] = useState("")
        const [mailmsg, setMailmsg] = useState("")
        const [passmsg, setPassmsg] = useState("")
        //Misc Variables (used for API response)
        var obj = []

        const handleSubmit = (event) => {
            setLoading(true);
            event.preventDefault()
            login({email:email,password:password})
                // Handle response
                .then(res => {
                    setLoading(false)
                    setToken(res.data.access_token);
                    setAdmin(res.data.admin);
                    console.log(res.data.admin === "admin")
                    if(res.data.admin === "admin"){
                        history.push('/admin/generate-code');
                    }else{
                        history.push('/user/dashboard');
                    }
                })
                .catch(err => {
                    setLoading(false);
                })
        }
        return (
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                </Avatar>
                <Typography component="h1" variant="h5">
                    Log in
                </Typography>
                <Box mt={3}>
                    <form id="loginForm" className={classes.form} onSubmit={handleSubmit}>
                        <TextField
                            id="email"
                            type="email"
                            name="email"
                            value={email}
                            placeholder="Email"
                            size="small"
                            variant="outlined"
                            className={classes.root}
                            InputProps={{
                                className: classes.input
                            }}
                            onChange={(event) => {
                                setEmail(event.target.value)
                            }}
                            fullWidth
                            required
                        />

                        {mailmsg && <Typography className={classes.error}>{mailmsg}</Typography>}
                        <TextField
                            id="password"
                            type="password"
                            name="password"
                            value={password}
                            placeholder="Password"
                            size="small"
                            variant="outlined"
                            className={classes.root}
                            InputProps={{
                                className: classes.input
                            }}
                            onChange={(event) => {
                                setPassword(event.target.value)
                            }}
                            fullWidth
                            required
                        />
                        {passmsg && <Typography className={classes.error}>{passmsg}</Typography>}

                        <ThemeProvider theme={theme}>
                        <FormControlLabel
                            control={<Checkbox value="remember" className={classes.radio} color="primary" />}
                            label="Remember me"
                        />
                        </ThemeProvider>
                        <Button
                            disabled={loading}
                            type="submit"
                            fullWidth
                            variant="contained"
                            className={classes.submit}
                        >
                            {loading ?
                                <CircularProgress size={24} variant="indeterminate" disableShrink={true} /> :
                                <Typography style={{color:"white"}}>Login</Typography>}
                        </Button>
                        <Grid container>
                            <Grid item xs>
                                <a href="#" onClick={() => { Redirect('reset') }}> Reset password</a>
                            </Grid>
                            <Grid item>
                                <a href="#" onClick={() => { Redirect('register') }}>Create an account</a>
                            </Grid>
                        </Grid>
                        </form>
                </Box>

            </div>
        )
}

export default Login