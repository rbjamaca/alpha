import { AppBar, Avatar, Divider, FormControl, Grid, Icon, IconButton, InputLabel, makeStyles, MenuItem, NativeSelect, Paper, Select, Typography, withStyles } from '@material-ui/core'
import { green, red } from '@material-ui/core/colors';
import React, { useEffect, useState } from 'react'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import InputBase from '@material-ui/core/InputBase';
import { fetchUser, fetchDownlines, register, fetchUserId, userSearchById } from '../../../data/api';
import useForm from '../../../data/useForm';
import { PhotoSizeSelectLargeOutlined } from '@material-ui/icons';
import { useSnackbar } from 'notistack';


const BootstrapInput = withStyles((theme) => ({
    root: {
        'label + &': {
            marginTop: 7,
        },
    },
    input: {
        borderRadius: 4,
        position: 'relative',
        backgroundColor: theme.palette.background.paper,
        border: '1px solid #ced4da',
        fontSize: 16,
        padding: '10px 26px 10px 12px',
        transition: theme.transitions.create(['border-color', 'box-shadow']),
        // Use the system font instead of the default Roboto font.
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
        '&:focus': {
            borderRadius: 4,
            borderColor: '#80bdff',
            boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
        },
    },
}))(InputBase);


const useStyles = makeStyles((theme) => ({
    title: {
        color: red[700],
    },
    regheader: {
        backgroundColor: red[800],
    },
    regtitle: {
        color: theme.palette.common.white,
        margin: theme.spacing(1),
    },
    form: {
        paddingLeft: theme.spacing(4),
        paddingRight: theme.spacing(4),
    },
    formControl: {
        minWidth: 120,
    },
    large: {
        width: theme.spacing(7),
        height: theme.spacing(7),
    },
    userIcon: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
        width: theme.spacing(8),
        height: theme.spacing(8),
    },
    fullWidth: {
        height: "110vh",
        flexGrow: 1,
    },
}));

const MemberStructure = (props) => {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [user, setUser] = useState(null);
    const [downlines, setdownlines] = useState()
    const [id, setId] = useState(null);
    const [position, setPosition] = useState(null);
    const [refresh, setRefresh] = useState(false)
    const [other, setOther] = useState(false)
    const { form, handleChange, resetForm } = useForm({
        firstname: '',
        surname: '',
        midname: '',
        address: '',
        gender: '',
        civil: '',
        birthmonth: '',
        birthday: '',
        birthyear: '',
        cell: '',
        username: '',
        sponsor: '',
        code: '',
        email: '',
        password: '',
    });

    const getUser = async () => {
        fetchUser().then(res => {
            setUser(res.data.user)
            setdownlines(res.data.list)
            setOther(false)
        })
    }

    const getUserId = async (id) => {
        fetchUserId(id).then(res => {
            setdownlines(res.data.list)
            setOther(true)
        })
    }

    useEffect(() => {
        getUser()
    }, [refresh]);

    const loop = (data, size) => {
        const downline = data.nested_downlines;
        return [0,1].map(_ => downline[_] ? <HasDownline key={_} size={size} status={downline[_].user.status} user={downline[_].user} username={downline[_].user.username} num={downline[_].user.id_number} id={downline[_].user.id}/> : <HasNoDownline key={_} hasL={downline[0] ? true : false} size={size} id={data.id} position={_} />)
    }

    const blank = (data, size) => {
        return [0,1].map(_ => <Empty key={_} />)
    }

    const hasArray = (data) => {
        if (data.length < 1) {
            return [undefined, undefined]
        } 

        if (data.length > 0) {
            return [0,1].map(_ => data[_] ? data[_] : undefined)
        }
    }
    
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const warning = () => {
        enqueueSnackbar('Register on the Left first', {
            variant: "warning",
        })
    }

    const [idnum, setIdnum] = useState(null)
    const [pos, setPos] = useState(null)
    const search = () => {
        userSearchById(form.sponsor, id).then(res => {
            if(res.data.info){
                setIdnum(res.data.info);
                setPos(res.data.pos.position);
            }else{
                enqueueSnackbar("User does not exist", {
                    variant: "error",
                })
            }
            // setId(res.data.info.id);
        }).catch(res => {
            enqueueSnackbar("User does not exist", {
                variant: "error",
            })
        })
    }
    

    const handleSubmit = async (e) => {
        e.preventDefault();
        if(form.sponsor && idnum){
            form.sponsor = idnum.id_number;
            form.sponsor_id = id;
            form.position = position;
            form.pos = pos;
            await register(form).then(res => {
                console.log(res)
                enqueueSnackbar("Successfully registered member", {
                    variant: "success",
                })
                setIdnum(null)
                setPosition(null)
                setPos(null)
                resetForm();
                handleClose();
                setRefresh(!refresh)
            }).catch(err => {
                enqueueSnackbar(err.response.data.message, {
                    variant: "error",
                })
                
            })
        }else{
            enqueueSnackbar("Verify user first", {
                variant: "error",
            })
        }
        
    }

    const HasDownline = ({size, username, user, num, id, status}) => 
        <Grid item xs={size}>
            <Grid container justify="center" alignItems="center">
                <Grid item>
                    <Grid container justify="center" alignItems="center" direction="column">
                        <Grid item>
                            <IconButton onClick={()=>getUserId(id)}>
                            <Avatar variant="circular" src={status === "Gold" ? "/images/image7.png" : "/images/image5.png"} className={classes.userIcon} />
                            </IconButton>
                        </Grid>
                        <Grid item>
                            <Typography variant="body2">
                                {username}
                    </Typography>
                        </Grid>
                        <Grid item>
                            <Typography variant="body2">
                            {num}
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography variant="body2">
                                CD {user.cd_left} | {user.cd_right}
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography variant="body2">
                                FR {user.fr_left} | {user.fr_right}
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography variant="body2">
                                PR {user.pr_left} | {user.pr_right}
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography variant="body2">
                                GL {user.gl_left} | {user.gl_right}
                            </Typography>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>

    const HasNoDownline = ({size, id, position, hasL}) => 
    <Grid item xs={size}>
        <Grid container justify="center" alignItems="center">
            <Grid item>
                <Grid container justify="center" alignItems="center" direction="column">
                    <Grid item>
                        
                        {hasL ?
                        <IconButton onClick={()=>handleClickOpen(id, position)}>
                        <Avatar variant="circular" src="/images/image10.png" className={classes.userIcon} />
                        </IconButton>
                        :
                        position === 0 ?
                        <IconButton onClick={()=>handleClickOpen(id, position)}>
                        <Avatar variant="circular" src="/images/image10.png" className={classes.userIcon} />
                        </IconButton>
                        :
                        <IconButton onClick={warning}>
                        <Avatar variant="circular" src="/images/image10.png" className={classes.userIcon} />
                        </IconButton>
                        }
                    </Grid>
                    <Grid item>
                        <Typography variant="body2">
                            {/* {username} */} 
                </Typography>
                    </Grid>
                    <Grid item>
                        <Typography variant="body2">
                        {/* AM-1137-{id-1} */} 
                </Typography>
                    </Grid>
                    <Grid item>
                        <Typography variant="body2">
                            {/* L=1:R=1 */} 
                </Typography>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    </Grid>
    
    const Empty = () =>
        <Grid item xs={1}>
            <Grid container justify="center" alignItems="center">
                <Grid item>
                    <Grid container justify="center" alignItems="center" direction="column">
                        
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    

    const handleClickOpen = (_id, _position) => {
        const pos = _position === 0 ? 'L' : 'R'
        setId(_id);
        setPosition(pos)
        setOpen(true);
    };

    const handleClose = () => {
        setId(null);
        setPosition(null);
        setOpen(false);
    };

    return (
        <div className={classes.fullWidth}>
            <Typography component="div" variant="h3" className={classes.title}>
                <Grid container alignItems="flex-start" justify="space-between">
                    <Grid>
                        <i>Member Structure</i>
                    </Grid>
                    <Grid>
                    {other &&
                        <IconButton onClick={getUser}>
                            
                            <Icon style={{color:green[500]}}>
                                refresh
                            </Icon>
                        </IconButton>
                    }
                    </Grid>
                </Grid>
            </Typography>
            <Grid container direction="row">
                <Grid item xs={12}>
                    <Grid container justify="center" alignItems="center">
                        <Grid item>
                            {downlines &&
                                <Grid container justify="center" alignItems="center" direction="column">
                                    <Grid item>
                                        <Avatar variant="circular" src={downlines.status === "Gold" ? "/images/image7.png" : "/images/image5.png"} className={classes.userIcon} />
                                    </Grid>
                                    <Grid item>
                                        <Typography variant="body2">
                                            {downlines.username}
                                        </Typography>
                                    </Grid>
                                    <Grid item>
                                        <Typography variant="body2">
                                            {downlines.id_number}
                                        </Typography>
                                    </Grid>
                                    <Grid item>
                                        <Typography variant="body2">
                                            CD {downlines.cd_left} | {downlines.cd_right}
                                        </Typography>
                                    </Grid>
                                    <Grid item>
                                        <Typography variant="body2">
                                            FR {downlines.fr_left} | {downlines.fr_right}
                                        </Typography>
                                    </Grid>
                                    <Grid item>
                                        <Typography variant="body2">
                                            PR {downlines.pr_left} | {downlines.pr_right}
                                        </Typography>
                                    </Grid>
                                    <Grid item>
                                        <Typography variant="body2">
                                            GL {downlines.gl_left} | {downlines.gl_right}
                                        </Typography>
                                    </Grid>
                                </Grid>
                            }
                        </Grid>
                    </Grid>
                </Grid>
                {downlines &&
                <>
                {
                    loop(downlines, 6)
                }
                {
                    <>
                    {downlines.nested_downlines[0] && loop(downlines.nested_downlines[0].user, 3)}
                    {downlines.nested_downlines[1] && loop(downlines.nested_downlines[1].user, 3)}
                    </>
                }
                {
                    <Grid item xs={12}>
                    <Grid container justify="space-evenly">
                    {downlines.nested_downlines[0] && downlines.nested_downlines[0].user.nested_downlines.length > 0 ?
                      <>
                      {downlines.nested_downlines[0].user.nested_downlines[0] ? loop(downlines.nested_downlines[0].user.nested_downlines[0].user, 1) : blank([undefined, undefined], 1)}
                      {downlines.nested_downlines[0].user.nested_downlines[1] ? loop(downlines.nested_downlines[0].user.nested_downlines[1].user, 1) : blank([undefined, undefined], 1)}
                      </>
                      : 
                      <>
                      {blank([undefined, undefined], 1)}
                      {blank([undefined, undefined], 1)}
                      </>
                    }
                    {downlines.nested_downlines[1] && downlines.nested_downlines[1].user.nested_downlines.length > 0 ?
                      <>
                      {downlines.nested_downlines[1].user.nested_downlines[0] ? loop(downlines.nested_downlines[1].user.nested_downlines[0].user, 1) : blank([undefined, undefined], 1)}
                      {downlines.nested_downlines[1].user.nested_downlines[1] ? loop(downlines.nested_downlines[1].user.nested_downlines[1].user, 1) : blank([undefined, undefined], 1)}
                      </>
                      : 
                      <>
                      {blank([undefined, undefined], 1)}
                      {blank([undefined, undefined], 1)}
                      </>
                    }
                    </Grid>
                    </Grid>
                    }
                </>
                }
                {/* {downlines && downlines.length > 0 ?
                    <>
                        <Grid item xs={6}>
                            <Grid container justify="center" alignItems="center">
                                <Grid item>
                                    <Grid container justify="center" alignItems="center" direction="column">
                                        <Grid item>
                                            <Avatar variant="circular" src="/images/image5.png" className={classes.userIcon} />
                                        </Grid>
                                        <Grid item>
                                            <Typography variant="body2">
                                                {downlines[0].user.username}
                                    </Typography>
                                        </Grid>
                                        <Grid item>
                                            <Typography variant="body2">
                                            AM-1137-{downlines[0].user.id-1}
                                    </Typography>
                                        </Grid>
                                        <Grid item>
                                            <Typography variant="body2">
                                    </Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                        {downlines[1] ?
                        <Grid item xs={6}>
                         <Grid container justify="center" alignItems="center">
                             <Grid item>
                                 <Grid container justify="center" alignItems="center" direction="column">
                                     <Grid item>
                                         <Avatar variant="circular" src="/images/image5.png" className={classes.userIcon} />
                                     </Grid>
                                     <Grid item>
                                         <Typography variant="body2">
                                             MasterCoffee2020
                                 </Typography>
                                     </Grid>
                                     <Grid item>
                                         <Typography variant="body2">
                                             AM-1137-3
                                 </Typography>
                                     </Grid>
                                     <Grid item>
                                         <Typography variant="body2">
                                             L=1:R=1
                                 </Typography>
                                     </Grid>
                                 </Grid>
                             </Grid>
                         </Grid>
                        </Grid>
                        :
                        <Grid item xs={6}>
                            <Grid container justify="center" alignItems="center">
                                <Grid item>
                                    <Grid container justify="center" alignItems="center" direction="column">
                                        <Grid item>
                                            <IconButton onClick={()=>handleClickOpen(user.id, 'R')}>
                                                <Avatar variant="circular" src="/images/image10.png" className={classes.userIcon} />
                                            </IconButton>
                                        </Grid>
                                        <Grid item>
                                            <Typography variant="body2">
                                            </Typography>
                                        </Grid>
                                        <Grid item>
                                            <Typography variant="body2">
                                            </Typography>
                                        </Grid>
                                        <Grid item>
                                            <Typography variant="body2">
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                        
                        }
                       
                    </>
                    :
                    <>
                        <Grid item xs={6}>
                            <Grid container justify="center" alignItems="center">
                                <Grid item>
                                    <Grid container justify="center" alignItems="center" direction="column">
                                        <Grid item>
                                            <IconButton onClick={()=>handleClickOpen(user.id, 'L')}>
                                                <Avatar variant="circular" src="/images/image10.png" className={classes.userIcon} />
                                            </IconButton>
                                        </Grid>
                                        <Grid item>
                                            <Typography variant="body2">
                                            </Typography>
                                        </Grid>
                                        <Grid item>
                                            <Typography variant="body2">
                                            </Typography>
                                        </Grid>
                                        <Grid item>
                                            <Typography variant="body2">
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={6}>
                            <Grid container justify="center" alignItems="center">
                                <Grid item>
                                    <Grid container justify="center" alignItems="center" direction="column">
                                        <Grid item>
                                            <IconButton onClick={()=>handleClickOpen(user.id, 'R')}>
                                                <Avatar variant="circular" src="/images/image10.png" className={classes.userIcon} />
                                            </IconButton>
                                        </Grid>
                                        <Grid item>
                                            <Typography variant="body2">
                                            </Typography>
                                        </Grid>
                                        <Grid item>
                                            <Typography variant="body2">
                                            </Typography>
                                        </Grid>
                                        <Grid item>
                                            <Typography variant="body2">
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </>
                } */}

                {/* <Grid item xs={3}>
                    <Grid container justify="center" alignItems="center">
                        <Grid item>
                            <Grid container justify="center" alignItems="center" direction="column">
                                <Grid item>
                                    <Avatar variant="circular" src="/images/image5.png" className={classes.userIcon} />
                                </Grid>
                                <Grid item>
                                    <Typography variant="body2">
                                        MasterCoffee2020
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <Typography variant="body2">
                                        AM-1137-4
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <Typography variant="body2">
                                        L=0:R=0
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={3}>
                    <Grid container justify="center" alignItems="center">
                        <Grid item>
                            <Grid container justify="center" alignItems="center" direction="column">
                                <Grid item>
                                    <Avatar variant="circular" src="/images/image5.png" className={classes.userIcon} />
                                </Grid>
                                <Grid item>
                                    <Typography variant="body2">
                                        MasterCoffee2020
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <Typography variant="body2">
                                        AM-1137-5
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <Typography variant="body2">
                                        L=0:R=0
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={3}>
                    <Grid container justify="center" alignItems="center">
                        <Grid item>
                            <Grid container justify="center" alignItems="center" direction="column">
                                <Grid item>
                                    <Avatar variant="circular" src="/images/image5.png" className={classes.userIcon} />
                                </Grid>
                                <Grid item>
                                    <Typography variant="body2">
                                        MasterCoffee2020
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <Typography variant="body2">
                                        AM-1137-6
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <Typography variant="body2">
                                        L=0:R=0
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={3}>
                    <Grid container justify="center" alignItems="center">
                        <Grid item>
                            <Grid container justify="center" alignItems="center" direction="column">
                                <Grid item>
                                    <Avatar variant="circular" src="/images/image5.png" className={classes.userIcon} />
                                </Grid>
                                <Grid item>
                                    <Typography variant="body2">
                                        MasterCoffee2020
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <Typography variant="body2">
                                        AM-1137-7
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <Typography variant="body2">
                                        L=0:R=0
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid> */}
                {/* Last Row */}
                {/* <Grid item xs={12}>
                    <Grid container justify="space-evenly">
                        <Grid item xs={1}>
                            <Grid container justify="center" alignItems="center">
                                <Grid item>
                                    <Grid container justify="center" alignItems="center" direction="column">
                                        <Grid item>
                                            <IconButton onClick={handleClickOpen}>
                                                <Avatar variant="circular" src="/images/image10.png" className={classes.userIcon} />
                                            </IconButton>
                                        </Grid>
                                        <Grid item>
                                            <Typography variant="body2">
                                               
                                            </Typography>
                                        </Grid>
                                        <Grid item>
                                            <Typography variant="body2">

                                            </Typography>
                                        </Grid>
                                        <Grid item>
                                            <Typography variant="body2">

                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={1}>
                            <Grid container justify="center" alignItems="center">
                                <Grid item>
                                    <Grid container justify="center" alignItems="center" direction="column">
                                        <Grid item>
                                            <IconButton onClick={handleClickOpen}>
                                                <Avatar variant="circular" src="/images/image10.png" className={classes.userIcon} />
                                            </IconButton>
                                        </Grid>
                                        <Grid item>
                                            <Typography variant="body2">

                                            </Typography>
                                        </Grid>
                                        <Grid item>
                                            <Typography variant="body2">

                                            </Typography>
                                        </Grid>
                                        <Grid item>
                                            <Typography variant="body2">

                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={1}>
                            <Grid container justify="center" alignItems="center">
                                <Grid item>
                                    <Grid container justify="center" alignItems="center" direction="column">
                                    
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={1}>
                            <Grid container justify="center" alignItems="center">
                                <Grid item>
                                    <Grid container justify="center" alignItems="center" direction="column">
                                        
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={1}>
                            <Grid container justify="center" alignItems="center">
                                <Grid item>
                                    <Grid container justify="center" alignItems="center" direction="column">
                                        
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={1}>
                            <Grid container justify="center" alignItems="center">
                                <Grid item>
                                    <Grid container justify="center" alignItems="center" direction="column">
                                       
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={1}>
                            <Grid container justify="center" alignItems="center">
                                <Grid item>
                                    <Grid container justify="center" alignItems="center" direction="column">
                                       
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={1}>
                            <Grid container justify="center" alignItems="center">
                                <Grid item>
                                    <Grid container justify="center" alignItems="center" direction="column">
                                        
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid> */}

            </Grid>
            {/* <Button variant="outlined" color="primary" onClick={handleClickOpen}>
                Open form dialog
            </Button> */}
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title"
                fullWidth={true}
                maxWidth="sm"
            >
                <Paper className={classes.regheader}>
                    <Typography variant="h5" className={classes.regtitle}>
                        Online Registration Form
                    </Typography>
                </Paper>
                <form onSubmit={handleSubmit}>
                <DialogContent className={classes.form}>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="surname"
                        variant="outlined"
                        label="Surname"
                        type="text"
                        name="surname"
                        value={form.surname}
                        onChange={handleChange}
                        fullWidth
                        required
                    />
                    <TextField
                        margin="dense"
                        id="firstname"
                        variant="outlined"
                        label="First Name"
                        name="firstname"
                        value={form.firstname}
                        onChange={handleChange}
                        type="text"
                        fullWidth
                        required
                    />
                    <TextField
                        margin="dense"
                        id="midname"
                        variant="outlined"
                        label="Middle Name"
                        name="midname"
                        value={form.midname}
                        onChange={handleChange}
                        type="text"
                        fullWidth
                        required
                    />
                    <TextField
                        margin="dense"
                        id="address"
                        variant="outlined"
                        label="Home Address"
                        name="address"
                        value={form.address}
                        onChange={handleChange}
                        type="text"
                        multiline
                        rows={2}
                        fullWidth
                        style={{ marginBottom: 10 }}
                        required
                    />
                    <Grid container direction="column" spacing={1}>
                        <Grid item>
                            <FormControl variant="outlined" className={classes.formControl}>
                                <InputLabel>Gender</InputLabel>
                                <NativeSelect
                                    name="gender"
                                    value={form.gender}
                                    onChange={handleChange}
                                    // onChange={(e) => {}}
                                    label="Gender"
                                    input={<BootstrapInput />}
                                    required
                                >
                                    <option value="">
                                    </option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </NativeSelect>
                            </FormControl>
                        </Grid>
                        <Grid item>
                            <FormControl variant="outlined" className={classes.formControl}>
                                <InputLabel>Civil Status</InputLabel>
                                <NativeSelect
                                    value=""
                                    name="civil"
                                    value={form.civil}
                                    onChange={handleChange}
                                    // onChange={(e) => {}}
                                    label="Civil Status"
                                    input={<BootstrapInput />}
                                    required
                                >
                                    <option value="">
                                    </option>
                                    <option value="Single">Single</option>
                                    <option value="Married">Married</option>
                                    <option value="Divorced">Divorced</option>
                                    <option value="Widow">Widow</option>
                                </NativeSelect>
                            </FormControl>
                        </Grid>
                        <Grid item>
                            Birth Date
                    </Grid>
                        <Grid item>
                            <Grid container direction="row" spacing={1}>
                                <Grid item>
                                    <FormControl variant="outlined" className={classes.formControl}>
                                        <InputLabel>Month</InputLabel>
                                        <NativeSelect
                                            value=""
                                            name="birthmonth"
                                            value={form.birthmonth}
                                            onChange={handleChange}
                                            // onChange={(e) => {}}
                                            label="Month"
                                            input={<BootstrapInput />}
                                            required
                                        >
                                            <option value="">

                                            </option>
                                            <option value={1}>January</option>
                                            <option value={2}>February</option>
                                            <option value={3}>March</option>
                                            <option value={4}>April</option>
                                            <option value={5}>May</option>
                                            <option value={6}>June</option>
                                            <option value={7}>July</option>
                                            <option value={8}>August</option>
                                            <option value={9}>September</option>
                                            <option value={10}>October</option>
                                            <option value={11}>November</option>
                                            <option value={12}>December</option>
                                        </NativeSelect>
                                    </FormControl>
                                </Grid>
                                <Grid item>
                                    <TextField
                                        style={{ maxWidth: "10rem" }}
                                        margin="dense"
                                        id="day"
                                        variant="outlined"
                                        label="Day"
                                        name="birthday"
                                        value={form.birthday}
                                        onChange={handleChange}
                                        type="text"
                                        required
                                    />
                                </Grid>
                                <Grid item>
                                    <TextField
                                        style={{ maxWidth: "10rem" }}
                                        margin="dense"
                                        id="year"
                                        variant="outlined"
                                        label="Year"
                                        name="birthyear"
                                        value={form.birthyear}
                                        onChange={handleChange}
                                        type="text"
                                        required
                                    />
                                </Grid>
                            </Grid>



                        </Grid>
                    </Grid>
                    <TextField
                        margin="dense"
                        id="number"
                        variant="outlined"
                        label="Cell Number"
                        name="cell"
                        value={form.cell}
                        onChange={handleChange}
                        type="text"
                        fullWidth
                        required
                    />
                    <TextField
                        margin="dense"
                        id="username"
                        variant="outlined"
                        label="Username"
                        name="username"
                        value={form.username}
                        onChange={handleChange}
                        type="text"
                        required
                    />
                    <Divider style={{ margin: 5 }} />
                    <Grid item xs={12}>
                    <Grid container spacing={2}>
                        <Grid item>
                            <TextField
                                margin="dense"
                                id="sponsor"
                                variant="outlined"
                                label="Sponsor"
                                name="sponsor"
                                value={form.sponsor}
                                onChange={handleChange}
                                // defaultValue={downlines ? downlines.id_number : ""}
                                type="text"
                                required
                                // inputProps={{readOnly: true}}
                            />
                        </Grid>
                        <Grid item>
                            <Button style={{ marginTop: 10 }} onClick={search} size="medium" variant="contained" color="primary">
                                Verify
                            </Button>
                        </Grid>
                    </Grid>
                    </Grid>
                    {idnum && 
                    <Typography style={{padding:7, backgroundColor: "#b9b9b9", maxWidth: 210}}  variant="body1">
                        {idnum.firstname} {idnum.surname}    
                    </Typography>}


                    <TextField
                        margin="dense"
                        id="code"
                        variant="outlined"
                        label="Registration Code"
                        name="code"
                        value={form.code}
                        onChange={handleChange}
                        type="text"
                        fullWidth
                        required
                    />
                    <TextField
                        margin="dense"
                        id="email"
                        variant="outlined"
                        label="Email"
                        name="email"
                        value={form.email}
                        onChange={handleChange}
                        type="email"
                        fullWidth
                        required
                    />
                    <TextField
                        margin="dense"
                        id="password"
                        variant="outlined"
                        label="Password"
                        name="password"
                        value={form.password}
                        onChange={handleChange}
                        type="password"
                        fullWidth
                        required
                    />

                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
                </Button>
                    <Button type="submit" color="primary">
                        Register
                </Button>
                </DialogActions>
                </form>
            </Dialog>
        </div>
    )
}

export default MemberStructure
