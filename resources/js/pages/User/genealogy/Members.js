import { Button, Divider, FormControl, Grid, InputLabel, makeStyles, NativeSelect, Paper, TextField, Typography, withStyles } from '@material-ui/core';
import { red } from '@material-ui/core/colors';
import React, { useState, useEffect } from 'react'
import {API_RETRIEVE, fetchPurchases, saveCode, savePurchase, searchById} from '../../../data/api'
import useForm from '../../../data/useForm';
import InputBase from '@material-ui/core/InputBase';
import MembersTable from './MembersTable'
import { useSnackbar } from 'notistack';

const BootstrapInput = withStyles((theme) => ({
    root: {
        'label + &': {
            marginTop: 7,
        },
    },
    input: {
        borderRadius: 4,
        position: 'relative',
        backgroundColor: theme.palette.background.paper,
        border: '1px solid #ced4da',
        fontSize: 16,
        padding: '10px 26px 10px 12px',
        transition: theme.transitions.create(['border-color', 'box-shadow']),
        // Use the system font instead of the default Roboto font.
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
        '&:focus': {
            borderRadius: 4,
            borderColor: '#80bdff',
            boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
        },
    },
}))(InputBase);

const useStyles = makeStyles((theme) => ({
    title: {
        color: red[700],
    },
    regheader: {
        backgroundColor: red[800],
    },
    regtitle: {
        color: theme.palette.common.white,
        margin: theme.spacing(1),
    },
    form: {
        paddingLeft: theme.spacing(4),
        paddingRight: theme.spacing(4),
    },
    formControl: {
        minWidth: 210,
    },
    large: {
        width: theme.spacing(7),
        height: theme.spacing(7),
    },
    userIcon: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
        width: theme.spacing(8),
        height: theme.spacing(8),
    },
    fullWidth: {
        margin: theme.spacing(4),
        flexGrow: 1,
    },
}));

const Members = (props) => {
    const classes = useStyles();
    const [code, setCode] = useState("")
    const [preregCode, setpreregCode] = useState("")
    const [update, setUpdate] = useState(false);

    const [rows, setRows] = useState([])
    const [sets, setSets] = useState([])
    const [user, setUser] = useState(null)

    const getCodes = async () => {
        await API_RETRIEVE('/api/user/members').then(res => {
            setRows(res.data.list)
        })
    }

    // const search = () => {
    //     searchById(form.id_number).then(res => {
    //         setUser(res.data.info);
    //     })
    // }
    
    
    

    useEffect(() => {
        getCodes();
    }, [])
    

    return (
        <div className={classes.fullWidth}>
            <Grid container>
                <Grid item xs={12}>
                    <div style={{height:"90vh"}}>
                        <MembersTable rows={rows}/>
                    </div>
                </Grid>
            </Grid>
        </div>
    )
}

export default Members
