import { Button, Card, CardActions, CardContent, CardHeader, Divider, Grid, makeStyles, Paper, TextField, Typography } from '@material-ui/core';
import { deepOrange, green, orange, red } from '@material-ui/core/colors';
import React, { useState, useEffect } from 'react'
import {fetchCode, fetchMetrics, memberUpgrade} from '../../data/api'
import { useSnackbar } from 'notistack';

const useStyles = makeStyles((theme) => ({
    title: {
        color: red[700],
    },
    regheader: {
        backgroundColor: red[800],
    },
    regtitle: {
        color: theme.palette.common.white,
        margin: theme.spacing(1),
    },
    form: {
        paddingLeft: theme.spacing(4),
        paddingRight: theme.spacing(4),
    },
    formControl: {
        minWidth: 120,
    },
    large: {
        width: theme.spacing(7),
        height: theme.spacing(7),
    },
    userIcon: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
        width: theme.spacing(8),
        height: theme.spacing(8),
    },
    fullWidth: {
        marginTop: theme.spacing(15),
        margin: theme.spacing(10),
        flexGrow: 1,
    },
    cardReferral: {
        width: theme.spacing(40),
        background: `linear-gradient(to right, ${deepOrange[800]} 0%, ${orange[500]} 100%)`,
        color: theme.palette.common.white,
    },
    cardTotal: {
        width: theme.spacing(40),
        background: `linear-gradient(to right, ${red[800]} 30%, ${red[500]} 100%)`,
        color: theme.palette.common.white,
    },
    cardRemaining: {
        width: theme.spacing(40),
        background: `linear-gradient(to right, ${green[800]} 30%, ${green[400]} 100%)`,
        color: theme.palette.common.white,
    },
    card: {
        width: theme.spacing(40),
        background:
        `-moz-linear-gradient(
          -72deg,
          #ca7345,
          #ffdeca 16%,
          #ca7345 21%,
          #ffdeca 24%,
          #a14521 27%,
          #ca7345 36%,
          #ffdeca 45%,
          #ffdeca 60%,
          #ca7345 72%,
          #ffdeca 80%,
          #ca7345 84%,
          #732100
        )`,
        background:
        `-webkit-linear-gradient(
          -72deg,
          #ca7345,
          #ffdeca 16%,
          #ca7345 21%,
          #ffdeca 24%,
          #a14521 27%,
          #ca7345 36%,
          #ffdeca 45%,
          #ffdeca 60%,
          #ca7345 72%,
          #ffdeca 80%,
          #ca7345 84%,
          #732100
        )`,
        background:
        `-o-linear-gradient(
          -72deg,
          #ca7345,
          #ffdeca 16%,
          #ca7345 21%,
          #ffdeca 24%,
          #a14521 27%,
          #ca7345 36%,
          #ffdeca 45%,
          #ffdeca 60%,
          #ca7345 72%,
          #ffdeca 80%,
          #ca7345 84%,
          #732100
        )`,
        background:
        `linear-gradient(
          -72deg,
          #ca7345,
          #ffdeca 16%,
          #ca7345 21%,
          #ffdeca 24%,
          #a14521 27%,
          #ca7345 36%,
          #ffdeca 45%,
          #ffdeca 60%,
          #ca7345 72%,
          #ffdeca 80%,
          #ca7345 84%,
          #732100
        )`,
        // background: `linear-gradient(to right, ${deepOrange[800]} 0%, ${orange[500]} 100%)`,
        color: theme.palette.common.white,
    },
    cardPending: {
        width: theme.spacing(40),
        // background: `linear-gradient(to right, #f79d00, #64f38c)`,
        background:
        `-moz-linear-gradient(
        -72deg,
        #dedede,
        #ffffff 16%,
        #dedede 21%,
        #ffffff 24%,
        #454545 27%,
        #dedede 36%,
        #ffffff 45%,
        #ffffff 60%,
        #dedede 72%,
        #ffffff 80%,
        #dedede 84%,
        #a1a1a1
      )`,
      background:
      `-webkit-linear-gradient(
        -72deg,
        #dedede,
        #ffffff 16%,
        #dedede 21%,
        #ffffff 24%,
        #454545 27%,
        #dedede 36%,
        #ffffff 45%,
        #ffffff 60%,
        #dedede 72%,
        #ffffff 80%,
        #dedede 84%,
        #a1a1a1
      )`,
      background:
      `-o-linear-gradient(
        -72deg,
        #dedede,
        #ffffff 16%,
        #dedede 21%,
        #ffffff 24%,
        #454545 27%,
        #dedede 36%,
        #ffffff 45%,
        #ffffff 60%,
        #dedede 72%,
        #ffffff 80%,
        #dedede 84%,
        #a1a1a1
      )`,
      background:
      `linear-gradient(
        -72deg,
        #dedede,
        #ffffff 16%,
        #dedede 21%,
        #ffffff 24%,
        #454545 27%,
        #dedede 36%,
        #ffffff 45%,
        #ffffff 60%,
        #dedede 72%,
        #ffffff 80%,
        #dedede 84%,
        #a1a1a1
      )`,
        color: theme.palette.common.black,
    },
    cardGold: {
        width: theme.spacing(40),
        background: `radial-gradient(ellipse farthest-corner at right bottom, #FEDB37 0%, #FDB931 8%, #9f7928 30%, #8A6E2F 40%, transparent 80%),
                    radial-gradient(ellipse farthest-corner at left top, #FFFFFF 0%, #FFFFAC 8%, #D1B464 25%, #5d4a1f 62.5%, #5d4a1f 100%)`,
        color: theme.palette.common.white,
    }
}));

const DashboardContent = (props) => {
    const classes = useStyles();
    const [unilevel, setUnilevel] = useState(0);
    const [status, setStatus] = useState("");
    const [df, setDf] = useState(0)
    const [total, setTotal] = useState(0)
    const [members, setMembers] = useState(0)
    const [encashment, setEncashment] = useState(0)
    const [remaining, setRemaining] = useState(0)
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();
    const nf = new Intl.NumberFormat();
    // const recursiveMetrics = (data, _total, _count) => {
    //     let total = _total;
    //     let count = _count
    //     data.nested_downlines.map(_ => {
    //         console.log(total, count)
    //         if(_.user){
    //             _.user.purchases.map(_ => {
    //                 total += 10;
    //             })
    //         }
    //         count += 1;
    //         if (count >= 10){
    //             return false;
    //         }else{
    //             recursiveMetrics(_.user, total, count)
    //         }
    //     })
    // }
    useEffect(() => {
        setRemaining(total - encashment)
        return () => {

        }
    }, [total, encashment])
    
    const getMetrics = async () => {
        await fetchMetrics().then(async res => {
            const left = res.data.matching.left
            const right = res.data.matching.right
            const matching = (left > 0 && right > 0) ? (left > right ? right : left) * 1000 : 0
            setMembers(res.data.matching.total)
            setStatus(res.data.status)
            setDf(res.data.df + matching)
            console.log(res.data.df)
            const sum = res.data.df + matching
            let unl = 0;
            // recursiveMetrics(res.data.unilevel, 0, 0);
            if(res.data.code === "CD Code" || res.data.code === "Free Code"){
                unl = 0;
            }else{
                unl = res.data.unilevel.prem * 10 + res.data.unilevel.gold * 50;
            }
            setUnilevel(unl)
            console.log('sum', unl, sum)
            setTotal(unl + sum)
        })
    }

    const handleUpgrade = () => {
        memberUpgrade({status: 'Pending for Gold'}).then(res => {
            enqueueSnackbar("Your request for Gold Membership is now pending for admin approval", { 
                variant: 'success',
            });
            getMetrics();
        })
    }
    
    

    useEffect(() => {
        getMetrics();
    }, [])
    

    return (
        <div className={classes.fullWidth}>
            <Grid container direction="row" justify="space-evenly" alignItems="center" spacing={2}>
                <Grid item xs={6}>
                    <Grid container direction="column" alignItems="center" spacing={5}>
                        {/* <Grid item xs={4}>
                            <Grid container direction="column" alignItems="flex-start">
                            <Card className={status === "Gold" ? classes.cardPending : status === "Pending for Gold" ? classes.cardPending : classes.card}>
                                <CardHeader title="Unilevel Rewards"/>
                                <CardContent>
                                    <Typography align="center" variant="h4">
                                        {unilevel <= 5100 ? unilevel.toLocaleString('en-PH', {style: 'currency', currency: 'PHP'}) : (5100).toLocaleString('en-PH', {style: 'currency', currency: 'PHP'})}
                                    </Typography>
                                </CardContent>
                                {status === "Pre-Membership" && unilevel >= 5100 &&
                                <CardActions style={{display:'block'}}>
                                    <Typography component="div" align="center">
                                    <Button onClick={handleUpgrade} variant="contained" style={{background:"inherit"}} className="text-white">Upgrade to Gold</Button>
                                    </Typography>
                                </CardActions>
                                }
                            </Card>  
                            </Grid>
                        </Grid> */}

                        <Grid item xs={4}>
                            <Grid container direction="column" alignItems="flex-start">
                            {/* <Card className={status === "Gold" ? classes.cardGold : status === "Pending for Gold" ? classes.cardPending : classes.card}>
                                <CardHeader title="Total Unilevel Rewards"/>
                                <CardContent>
                                    <Typography align="center" variant="h4">
                                        &#8369; {unilevel.toFixed(2)}
                                    </Typography>
                                </CardContent>
                                {status === "Pre-Membership" && unilevel >= 5100 &&
                                <CardActions style={{display:'block'}}>
                                    <Typography component="div" align="center">
                                    <Button onClick={handleUpgrade} variant="contained" style={{background:"inherit"}} className="text-white">Upgrade to Gold</Button>
                                    </Typography>
                                </CardActions>
                                }
                            </Card>   */}
                                <Card className={classes.cardReferral}>
                                    <CardHeader title="Total Encashments"/>
                                    <CardContent>
                                        <Typography align="center" variant="h4">
                                            {(encashment).toLocaleString('en-PH', {style: 'currency', currency: 'PHP'})}
                                        </Typography>
                                    </CardContent>
                                </Card>
                            </Grid>
                        </Grid>

                        <Grid item xs={4}>
                            <Grid container direction="column" alignItems="flex-start">
                                <Card className={classes.cardTotal}>
                                    <CardHeader title="Total Rewards"/>
                                    <CardContent>
                                        <Typography align="center" variant="h4">
                                            {total.toLocaleString('en-PH', {style: 'currency', currency: 'PHP'})}
                                        </Typography>
                                    </CardContent>
                                </Card>
                            </Grid>
                        </Grid>

                            {/* <Grid item xs={4}>
                            <Grid container direction="column" alignItems="flex-start">
                                <Card className={classes.cardReferral}>
                                    <CardHeader title="Direct Referrals"/>
                                    <CardContent>
                                        <Typography align="center" variant="h4">
                                            &#8369; {df.toFixed(2)}
                                        </Typography>
                                    </CardContent>
                                </Card>  
                            </Grid>
                            </Grid> */}
                    </Grid>
                </Grid>

                <Grid item xs={6}>
                    <Grid container direction="column" alignItems="center" spacing={5}>
                        {/* <Grid item xs={4}>
                                <Grid container direction="column" alignItems="flex-start">
                                <Card className={classes.cardGold}>
                                    <CardHeader title="Gold Rewards"/>
                                    <CardContent>
                                        <Typography align="center" variant="h4">
                                            {status === "Gold" ? (unilevel - 5100).toLocaleString('en-PH', {style: 'currency', currency: 'PHP'}) : "Locked"}
                                        </Typography>
                                    </CardContent>
                                    {status === "Pre-Membership" && unilevel >= 5100 &&
                                    <CardActions style={{display:'block'}}>
                                        <Typography component="div" align="center">
                                        <Button onClick={handleUpgrade} variant="contained" style={{background:"inherit"}} className="text-white">Upgrade to Gold</Button>
                                        </Typography>
                                    </CardActions>
                                    }
                                </Card>  
                                </Grid>
                        </Grid> */}

                        <Grid item xs={4}>
                            <Grid container direction="column" alignItems="flex-start">
                                <Card className={classes.cardRemaining}>
                                    <CardHeader title="Remaining Rewards"/>
                                    <CardContent>
                                        <Typography align="center" variant="h4">
                                            {(remaining).toLocaleString('en-PH', {style: 'currency', currency: 'PHP'})}
                                        </Typography>
                                    </CardContent>
                                </Card>
                            </Grid>
                        </Grid>

                        <Grid item xs={4}>
                            <Grid container direction="column" alignItems="flex-start">
                                <Card className={classes.cardRemaining}>
                                    <CardHeader title={members > 199 ? "Achiever" : members < 100 ? "Member" : "Organizer"}/>
                                    <CardContent>
                                        <Typography align="center" variant="h4">
                                            {members}
                                        </Typography>
                                    </CardContent>
                                </Card>
                            </Grid>
                        </Grid>

                    </Grid>
                </Grid>
            </Grid>
 
          
            
        </div>
    )
}

export default DashboardContent
